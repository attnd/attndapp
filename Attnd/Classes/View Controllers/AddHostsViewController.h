//
//  AddHostsViewController.h
//  Attnd
//
//  Created by Jatin Harish on 26/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol addHostDelegate <NSObject>

-(void) getHostsArray :(NSMutableArray *)array;
@end

@interface AddHostsViewController : UIViewController<UISearchBarDelegate>



{
    IBOutlet UILabel *lbl_NoFriends;
    IBOutlet UIView *view_Bottom;
    IBOutlet UICollectionView *colView_AddHosts;
    IBOutlet UIButton *btnAddHosts;
    
}
@property (nonatomic,retain) id <addHostDelegate> hostDelegate;
@end
