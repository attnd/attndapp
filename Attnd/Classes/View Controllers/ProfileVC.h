//
//  ProfileVC.h
//  Attnd
//
//  Created by Gobinder Singh Sidhu on 4/10/17.
//  Copyright © 2017 Gobinder Sidhu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuartzCore/CALayer.h"
//#import "TwoBtnsTableViewCell.h"
//#import "ThreeBtnsTableViewCell.h"

#import "PhotosVC.h"
#import "FriendsVC.h"
#import "EventsViewController.h"
#import "ProfileCentreCollectionViewCell.h"


//@protocol ProfileDelegate;


@interface ProfileVC : UIViewController<UIPageViewControllerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{

    NSString *userType;
    NSString *userIDStr;
    NSString *str_btn_clicked;
    
    NSMutableArray *btnTypeArray;
    UIImagePickerController* imagePicker;
    
       
    IBOutlet UILabel *lbl_realName;
    IBOutlet UILabel *lbl_userName;
    IBOutlet UILabel *lbl_location;
    IBOutlet UILabel *lbl_age;
    IBOutlet UILabel *lbl_relationshipStatus;
    
}

@property (weak, nonatomic) IBOutlet UIButton *btn_addPic;

@property (weak, nonatomic) IBOutlet UIButton *btn_deletePic;

@property (weak, nonatomic) IBOutlet UIImageView *imgvw_addPic;

@property (weak, nonatomic) IBOutlet UIImageView *imgvw_deletePic;

@property (weak, nonatomic) IBOutlet UIImageView *imgView_socialClicked;
@property (weak, nonatomic) IBOutlet UIView *view_tblView;
@property (weak, nonatomic) IBOutlet UIView *view_social;

@property (weak, nonatomic) IBOutlet UIImageView *imageView_profilePic;

@property (weak, nonatomic) IBOutlet UIButton *btn_instagram;

@property (weak, nonatomic) IBOutlet UIButton *btn_twitter;
@property (weak, nonatomic) IBOutlet UIButton *btn_snapchat;


@property (weak, nonatomic) IBOutlet UIView *view_center;

@end


