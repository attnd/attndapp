//
//  SignUp1ViewController.h
//  Attnd
//
//  Created by Jatin Harish on 11/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUp1ViewController : UIViewController
{
    
   
    IBOutlet UITextField *txt_RealName;
    
    IBOutlet UITextField *txt_Username;
    
    
    IBOutlet UITextField *txt_Email;
    
    IBOutlet UITextField *txt_Password;
    
    
    IBOutlet UILabel *lbl_realNameError;
    
    IBOutlet UILabel *lbl_userNameError;
    
    IBOutlet UILabel *lbl_emailError;
    IBOutlet UILabel *lbl_PasswordError;
    
    
}
@end
