//
//  ChangeEmailViewController.h
//  Attnd
//
//  Created by Gobinder Singh Sidhu on 4/14/17.
//  Copyright © 2017 Gobinder Sidhu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangeEmailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txt_currentEmail;
@property (weak, nonatomic) IBOutlet UITextField *txt_newEmail;
@property (weak, nonatomic) IBOutlet UITextField *txt_Password;

@end
