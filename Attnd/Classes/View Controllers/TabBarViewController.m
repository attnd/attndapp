//
//  TabBarViewController.m
//  Attnd
//
//  Created by Jatin Harish on 11/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import "TabBarViewController.h"
#import "AddEventViewController.h"
#import "Common.h"

@interface TabBarViewController ()<UITabBarControllerDelegate>
{
    Common *objCommon;
}
@end

@implementation TabBarViewController
-(void)viewWillAppear:(BOOL)animated
{

    [self.navigationController setNavigationBarHidden:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
   
    objCommon = [Common getInstance];
    objCommon.fromwhichIndex = [NSString stringWithFormat:@"0"];
    self.delegate=self;
    //self.navigationItem.hidesBackButton = YES;
    
    UITabBarItem *tabBarItem0 = [self.tabBarController.tabBar.items objectAtIndex:0];
    
    [tabBarItem0 setImage:[[UIImage imageNamed:@"homeunselected.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem0 setSelectedImage:[[UIImage imageNamed:@"homeselected.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
       
}

//- (BOOL)tabBarController:(UITabBarController *)tbController shouldSelectViewController:(UIViewController *)viewController
//{
//    if (viewController == [tbController.viewControllers objectAtIndex:1] )
//    {
//        // Enable all but the last tab.
//        return NO;
//    }
//    
//    return YES;
//}

- (void)tabBarController:(UITabBarController *)tbController didSelectViewController:(UIViewController *)viewController
{
    if (viewController == [tbController.viewControllers objectAtIndex:0] )
    {
        objCommon.fromwhichIndex = [NSString stringWithFormat:@"0"];
        
        
        
    }
    
    if (viewController == [tbController.viewControllers objectAtIndex:1] )
    {
        AddEventViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"AddEventViewController"];
        UINavigationController *navigationController =
        [[UINavigationController alloc] initWithRootViewController:controller];
        [self presentViewController:navigationController
                           animated:YES
                         completion:^{
                             
                         }];
    
        //objCommon.fromwhichIndex = [NSString stringWithFormat:@"1"];
    }
    if (viewController == [tbController.viewControllers objectAtIndex:2] )
    {
        objCommon.fromwhichIndex = [NSString stringWithFormat:@"2"];
    }
    if (viewController == [tbController.viewControllers objectAtIndex:3] )
    {
        objCommon.fromwhichIndex = [NSString stringWithFormat:@"3"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
