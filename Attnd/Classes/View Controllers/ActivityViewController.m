//
//  ActivityViewController.m
//  Attnd
//
//  Created by Jatin Harish on 12/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import "ActivityViewController.h"
#import "ActivityTableViewCell.h"
#import "EventDetailViewController.h"
@interface ActivityViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation ActivityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

#pragma mark - UITable View Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4 ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //uppercell
    
        static NSString *CellIdentifier = @"activity";
        
        ActivityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[ActivityTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
    
    cell.imgView_ProfilePic.layer.cornerRadius=cell.imgView_ProfilePic.frame.size.height/2;
    cell.imgView_ProfilePic.layer.masksToBounds = YES;
   // cell.imgView_ProfilePic.backgroundColor= [UIColor redColor];
    
    
    cell.lbl_description.text = @"Jatin Attending Abc event at JW Marriot";
    
    //cell.imgView_EventPic.backgroundColor = [UIColor blueColor];
    cell.imgView_EventPic.layer.cornerRadius = 5.0;
    
    
    
    
    
    return cell;
 
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
       

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
