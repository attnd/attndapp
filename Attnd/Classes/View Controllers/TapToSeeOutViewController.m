//
//  TapToSeeOutViewController.m
//  Attnd
//
//  Created by Jatin Harish on 12/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import "TapToSeeOutViewController.h"
#import "TapToSeeOutCollectionViewCell.h"
#import "Constant.h"
#import "Common.h"
#import "MBProgressHUD.h"
#import "friendListModal.h"
#import "Common.h"
#import "CallAPIClass.h"

@interface TapToSeeOutViewController ()<WebAPIDelegate>
{
    NSMutableArray *arrayfriends,*arrayfiltered,*arrayCheckBox;
    BOOL searchActive;
    CallAPIClass *objAPI;
}

@end

@implementation TapToSeeOutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    objAPI = [[CallAPIClass alloc] init];
    objAPI.delegate = self;
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    lbl_AlertMessage.layer.cornerRadius = 5.0;
    lbl_AlertMessage.layer.masksToBounds = YES;
    
    
//    if (!arrayfriends)
//    {
//        lbl_NoFriends.hidden = NO;
//        lbl_NoFriends.text = @"No Friends";
//    }
    
    arrayfiltered = [[NSMutableArray alloc]init];
    
    arrayCheckBox = [[NSMutableArray alloc]init];
    arrayfriends = [[NSMutableArray alloc]init];
    searchActive = NO;
    [self getfriendList];
    

}
- (void)searchBar:(UISearchBar *) searchBar textDidChange:(NSString *)searchText
{
    [arrayfiltered removeAllObjects];
    if (searchBar.text && [searchBar.text length])
    {
        searchActive = YES;
        
        NSMutableArray * arrFiltered = [[NSMutableArray alloc] init];
        for (int i = 0; i< arrayfriends.count; i++)
        {
            
            NSDictionary *tempdict  = [arrayfriends objectAtIndex:i];
            [arrFiltered addObject:tempdict];
        }
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",searchText];
        
        NSArray *filteredarray = [[arrFiltered valueForKey:@"friend_name"] filteredArrayUsingPredicate:predicate];
        
        if ([filteredarray count]==0)
        {
            [colViewTapFriends reloadData];
            
            [[Common sharedCommon] showAlertWithOkButton:@"" message:@"No Friends Found"];
            
        }
        for (int arrCount1=0; arrCount1<filteredarray.count; arrCount1++)
        {
            
            for (int arrCount=0; arrCount<arrayfriends.count; arrCount++)
            {
                NSString *tempstring = [NSString stringWithFormat:@"%@",[[arrayfriends objectAtIndex:arrCount] valueForKey:@"friend_name"]];
                
                NSLog(@"===%@",tempstring);
                
                NSString *comparestring = [filteredarray objectAtIndex:arrCount1];
                
                NSLog(@"===%@",comparestring);
                
                if([tempstring isEqualToString:comparestring])
                {
                    NSMutableDictionary *tempdict = [arrayfriends objectAtIndex:arrCount];
                    
                    [arrayfiltered addObject:tempdict];
                    //[colView_AddHosts reloadData];
                }
                else
                {
                    NSLog(@"xcfsc");
                }
                [colViewTapFriends reloadData];
            }
            
        }
    }
    else
    {
        searchActive = NO;
        
    }
    [colViewTapFriends reloadData];
    
    
}
- (IBAction)btnActionSendTaps:(UIButton *)sender
{
    
    
    
}

#pragma mark - Collection View Delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
{
    if (searchActive)
    {
        return arrayfiltered.count;
    }
    else
    {
        return arrayfriends.count;
    }

}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    TapToSeeOutCollectionViewCell *cell = (TapToSeeOutCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"TapToSeeOut" forIndexPath:indexPath];
    
    //Cornrt Raduis Set
    cell.imgView_FriendDP.layer.cornerRadius=cell.imgView_FriendDP.frame.size.height/2;
    cell.imgView_FriendDP.layer.masksToBounds = YES;
    cell.imgView_TickMark.layer.cornerRadius=cell.imgView_TickMark.frame.size.height/2;
    cell.imgView_TickMark.layer.masksToBounds = YES;

    
    [cell.btnTickMark addTarget:self action:@selector(ischecked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    //*-*-*-* set friend name according to search Active or Not
    if (searchActive)
    {
        friendListModal *objModal=[arrayfiltered objectAtIndex:indexPath.row];
        // cell.imgView_FriendPic.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kImgBaseURL,objModal.friend_ProfilePic]]]];
        NSMutableArray * matches = [[NSMutableArray alloc] init];
        for (int i = 0; i< arrayCheckBox.count; i++){
            [matches insertObject:[arrayCheckBox objectAtIndex:i] atIndex:i];
        }
        
        NSLog(@"%@",matches);
        if ([matches containsObject:objModal.friend_id])
        {
            cell.imgView_TickMark.image=  [UIImage imageNamed:@"tap_trans"];
        }
        else
        {
            cell.imgView_TickMark.image=  [UIImage imageNamed:@""];
            
        }
        
        cell.lbl_friendName.text = objModal.friend_name;
        
    }
    else
    {
        friendListModal *objModal=[arrayfriends objectAtIndex:indexPath.row];
        //cell.imgView_FriendPic.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kImgBaseURL,objModal.friend_ProfilePic]]]];
        NSMutableArray * matches = [[NSMutableArray alloc] init];
        for (int i = 0; i< arrayCheckBox.count; i++){
            [matches insertObject:[arrayCheckBox objectAtIndex:i] atIndex:i];
        }
        
        NSLog(@"%@",matches);
        if ([matches containsObject:objModal.friend_id])
        {
            cell.imgView_TickMark.image=  [UIImage imageNamed:@"tap_trans"];
        }
        else
        {
            cell.imgView_TickMark.image=  [UIImage imageNamed:@""];
            
        }
        
        cell.lbl_friendName.text = objModal.friend_name;
        
    }
    
    
    cell.btnTickMark.tag = indexPath.row;
    
    
    
    return cell;

}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/4 -10 , 120);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    
    
    [[Common sharedCommon] showAlertWithOkButton:@"" message:[NSString stringWithFormat:@"Event Added Successfully"]];
    
    
}

#pragma mark - Button Action
- (IBAction)btnActionBack:(UIBarButtonItem *)sender
{
    NavigationBack
    
}


- (IBAction)btnActionOk:(UIButton *)sender
{
    view_MessageToTap.hidden=YES;
}
- (IBAction)ischecked:(UIButton *)sender
{
    
    TapToSeeOutCollectionViewCell * cell = (TapToSeeOutCollectionViewCell *)[[sender superview] superview];
    NSIndexPath *indexPath = [colViewTapFriends indexPathForCell:cell];
    NSLog(@"%ld",indexPath.row);
    
    UIButton *btn=(UIButton*)sender;
    btn.selected = !btn.selected;
    
    if (searchActive)
    {
        friendListModal *objModal=[arrayfiltered objectAtIndex:indexPath.row];
        if (btn.selected)
        {
            
            cell.imgView_TickMark.image=  [UIImage imageNamed:@"tap_trans"];
            [arrayCheckBox addObject:objModal.friend_id];
            
        }
        else
        {
            cell.imgView_TickMark.image=  [UIImage imageNamed:@""];
            
            [arrayCheckBox removeObject:objModal.friend_id];
            
        }
        
    }
    else{
        
        friendListModal *objModal=[arrayfriends objectAtIndex:indexPath.row];
        if (btn.selected)
        {
            
            cell.imgView_TickMark.image=  [UIImage imageNamed:@"tap_trans"];
            [arrayCheckBox addObject:objModal.friend_id];
            
        }
        else
        {
            cell.imgView_TickMark.image=  [UIImage imageNamed:@""];
            [arrayCheckBox removeObject:objModal.friend_id];
            
        }
    }
}

-(void)getfriendList
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strConnectionURL = [NSString stringWithFormat:@"%@",GETFRIENDLIST];
    
    NSString *authorizationToken = [NSString stringWithFormat:@"BearerAttnd %@",SecurityToken];
    
    [objAPI callServerAPIwithHeader:nil ServiceType:@"GET" ServiceURL:strConnectionURL AutherisationToken:authorizationToken];
    
}
-(void)responseServerDataWithHeader:(NSDictionary *)responcedict
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"success"])
    {
        [arrayfriends removeAllObjects];
        
        //fetching array from Api
        NSMutableArray *fetched_Array= [[responcedict valueForKey:@"resData"] valueForKey:@"friends"];
        
        if (fetched_Array.count == 0)
        {
            lbl_NoFriends.hidden = NO;
            lbl_NoFriends.text = @"No Friends";
        }
        
        if (fetched_Array.count>0) {
            
            for (int i=0; i<fetched_Array.count; i++)
            {
                NSMutableDictionary *fetched_Dic=[fetched_Array objectAtIndex:i];
                friendListModal *objModal=[[friendListModal alloc]init];
                objModal= [objModal initWithDictionary:fetched_Dic];
                
                // Adding The model Obj to Final Array
                [arrayfriends addObject:objModal];
            }
            [colViewTapFriends reloadData];
            
            
        }
        if ([[responcedict valueForKey:@"resStatus"] isEqual:@"error"])
        {
            [[Common sharedCommon] showAlertWithOkButton:@"" message:[NSString stringWithFormat:@"%@",[responcedict valueForKey:@"resMessage"]]];
        }
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
