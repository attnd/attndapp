//
//  ChangeProfileDataViewController.m
//  Attnd
//
//  Created by Jatin Harish on 02/05/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import "ChangeProfileDataViewController.h"
#import "Constant.h"
#import "MBProgressHUD.h"
#import "CallAPIClass.h"
#import "Common.h"


#define textfieldBorderColor [UIColor colorWithRed:0.6431 green:0.7098 blue:0.8353 alpha:1.0].CGColor;


@interface ChangeProfileDataViewController ()<WebAPIDelegate>
{
    CallAPIClass *objAPI;
}

@end

@implementation ChangeProfileDataViewController
@synthesize titleSetting;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    objAPI = [[CallAPIClass alloc]init];
    objAPI.delegate = self;
    
    txt_Feild_ChangeData.layer.borderWidth = 1.0;
    txt_Feild_ChangeData.layer.borderColor = textfieldBorderColor;
    txt_Feild_ChangeData.layer.cornerRadius = 4.0;
    txt_Feild_ChangeData.layer.masksToBounds = YES;
    
    
    if ([titleSetting isEqualToString:@"location"])
    {
        self.title = @"Change Location";
        lbl_Placeholder.text = @"Change Location";
        
    }
    else if ([titleSetting isEqualToString:@"age"])
    {
        self.title = @"Change Age";
        lbl_Placeholder.text = @"Change Age";
    }
    else if ([titleSetting isEqualToString:@"status"])
    {
        self.title = @"Change Status";
        lbl_Placeholder.text = @"Change Status";
    }
    else if ([titleSetting isEqualToString:@"Instagram"])
    {
        self.title = @"CHANGE SOCIALS";
        lbl_Placeholder.text = @"Change Instagram";
    }
    else if ([titleSetting isEqualToString:@"Twitter"])
    {
        self.title = @"CHANGE SOCIALS";
        lbl_Placeholder.text = @"Change Twitter";
    }
    else if ([titleSetting isEqualToString:@"Snapchat"])
    {
        self.title = @"CHANGE SOCIALS";
        lbl_Placeholder.text = @"Change Snapchat";
    }
}
#pragma  mark -Button Action

- (IBAction)btnActionSave:(UIButton *)sender
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strConnectionURL;
    NSMutableDictionary *changeParam;
    
    if ([titleSetting isEqualToString:@"location"])
    {
         strConnectionURL = [NSString stringWithFormat:@"%@", CHANGELOCATION];
         changeParam = [NSMutableDictionary dictionaryWithObjectsAndKeys:txt_Feild_ChangeData.text, @"location", nil];
    }
    else if ([titleSetting isEqualToString:@"age"])
    {
        strConnectionURL = [NSString stringWithFormat:@"%@",CHANGEAGE];
        changeParam = [NSMutableDictionary dictionaryWithObjectsAndKeys:txt_Feild_ChangeData.text, @"age", nil];
    }
    else if ([titleSetting isEqualToString:@"status"])
    {
         strConnectionURL = [NSString stringWithFormat:@"%@",CHANGESTATUS];
        changeParam = [NSMutableDictionary dictionaryWithObjectsAndKeys:txt_Feild_ChangeData.text, @"relationshipStatus", nil];
    }
    else if ([titleSetting isEqualToString:@"Instagram"])
    {
        strConnectionURL = [NSString stringWithFormat:@"%@",CHANGEINSTA];
        changeParam = [NSMutableDictionary dictionaryWithObjectsAndKeys:txt_Feild_ChangeData.text, @"instagramId", nil];
    }
    else if ([titleSetting isEqualToString:@"Twitter"])
    {
        strConnectionURL = [NSString stringWithFormat:@"%@",CHANGETWITTER];
        changeParam = [NSMutableDictionary dictionaryWithObjectsAndKeys:txt_Feild_ChangeData.text, @"twitterId", nil];
    }
    else if ([titleSetting isEqualToString:@"Snapchat"])
    {
        strConnectionURL = [NSString stringWithFormat:@"%@",CHANGESNAPCHAT];
        changeParam = [NSMutableDictionary dictionaryWithObjectsAndKeys:txt_Feild_ChangeData.text, @"snapchatId", nil];
    }

   
    NSString *authorizationToken = [NSString stringWithFormat:@"BearerAttnd %@",SecurityToken];
    
    
    [objAPI callServerAPIwithHeader:changeParam ServiceType:@"PUT" ServiceURL:strConnectionURL AutherisationToken:authorizationToken];

    
}
-(void)responseServerDataWithHeader:(NSDictionary *)responcedict
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    
    
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"success"])
    {
     [[Common sharedCommon] showAlertWithOkButton:@"" message:[NSString stringWithFormat:@"%@",[responcedict valueForKey:@"resMessage"]]];
        
        
       
    }
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"error"])
    {
         [[Common sharedCommon] showAlertWithOkButton:@"" message:[NSString stringWithFormat:@"%@",[responcedict valueForKey:@"resMessage"]]];
    }

    
    
}

- (IBAction)btnActionBack:(UIBarButtonItem *)sender {
    NavigationBack
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
