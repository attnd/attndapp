//
//  NotificationsViewController.m
//  Attnd
//
//  Created by Jatin Harish on 27/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import "NotificationsViewController.h"
#import "InvitesViewController.h"
#import "EventsViewController.h"
#import "CAPSPageMenu.h"

@interface NotificationsViewController ()
@property (nonatomic) CAPSPageMenu *pageMenu;
@end

@implementation NotificationsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
        
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    
    InvitesViewController *ActivityView = [self.storyboard instantiateViewControllerWithIdentifier:@"InvitesViewController"];
    ActivityView.title = @"INVITES";
    
    EventsViewController *EventView = [self.storyboard instantiateViewControllerWithIdentifier:@"EventsViewController"];
    
    EventView.title = @"MESSAGE";
    
    
    
    
    NSArray *controllerArray = @[ActivityView, EventView];
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionSelectionIndicatorColor: [UIColor colorWithRed:0.3333 green:0.5373 blue:0.9725 alpha:1.0],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:70.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"VarelaRound" size:13.0],
                                 CAPSPageMenuOptionMenuHeight: @(30.0),
                                 CAPSPageMenuOptionMenuItemWidth: @(self.view.frame.size.width/2),
                                 CAPSPageMenuOptionCenterMenuItems: @(YES)
                                 };
    
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 64, self.view.frame.size.width, self.view.frame.size.height) options:parameters];
    [self.view addSubview:_pageMenu.view];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
