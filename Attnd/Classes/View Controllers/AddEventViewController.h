//
//  AddEventViewController.h
//  Attnd
//
//  Created by Jatin Harish on 14/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddEventCollectionViewCell.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"

@interface AddEventViewController : UIViewController
{
    
    IBOutlet UIImageView *imgView_EventImage;
    
    IBOutlet UIView *view_Bottom;
    IBOutlet UIButton *btnmakeEventLive;
    
    IBOutlet UIButton *btnPublic;
    
    IBOutlet UIButton *btnPrivate;
    
    
    IBOutlet UITextField *txt_EventTitle;
    IBOutlet UITextField *txt_Address;
    IBOutlet UITextField *txt_Date;
    
    IBOutlet UITextField *txt_Time;
    
}


@end
