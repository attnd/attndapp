//
//  ContactSupportViewController.m
//  Attnd
//
//  Created by Gobinder Singh Sidhu on 4/15/17.
//  Copyright © 2017 Gobinder Sidhu. All rights reserved.
//

#import "ContactSupportViewController.h"

@interface ContactSupportViewController ()

@end

@implementation ContactSupportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    messageArray = [[NSMutableArray alloc]init];
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
    
    
    
    // Do any additional setup after loading the view.
}



#pragma mark- ------------------ Text Field Delegate ------------------

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    
    self.bottomViewBottomConstraint.constant = 0;
    
    [messageArray addObject:_txt_message.text];
    [_messageTableView reloadData];
    
    return YES;
}


#pragma mark- ------------------ Keyboard Hide ------------------
- (void)keyboardWillHide:(NSNotification *)n
{
    
    self.bottomViewBottomConstraint.constant = 0;
    
    [_txt_message resignFirstResponder];
     [messageArray addObject:_txt_message.text];
    [_messageTableView reloadData];

    

}

#pragma mark- ------------------ Keyboard Show ------------------
- (void)keyboardWillShow:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    self.bottomViewBottomConstraint.constant = keyboardSize.height;
    
    
}



- (IBAction)btn_sendMessage:(id)sender {
    
    
    self.bottomViewBottomConstraint.constant = 0;
    
    [_txt_message resignFirstResponder];
     [_messageTableView reloadData];
}


#pragma mark- ------------------Table View Data source------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:
(NSInteger)section{
    
    
    return messageArray.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:
(NSIndexPath *)indexPath{
    
    
    static NSString *CellIdentifier = @"messageCell";
    
    
           UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                                     CellIdentifier];
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
    
            cell.textLabel.text = [messageArray objectAtIndex:indexPath.row];
            cell.textLabel.textAlignment = NSTextAlignmentRight;;
    
        return cell;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
