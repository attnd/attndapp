//
//  PeopleYouMayKnowViewController.h
//  Attnd
//
//  Created by Jatin Harish on 11/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PeopleYouMayKnowCollViewCell.h"
#import "UIImageView+WebCache.h"
#import "TabBarViewController.h"
#import "CallAPIClass.h"
#import "MBProgressHUD.h"
#import "Constant.h"
#import "AppDelegate.h"

@interface PeopleYouMayKnowViewController : UIViewController
{

//Linking for validation Alert View

    IBOutlet UIView *view_ValidationAlert;


    IBOutlet UILabel *lbl_ValidationAlert;

    IBOutlet UILabel *lbl_AlertMessage;
    
    IBOutlet UICollectionView *collView_PeopleYouMayKnow;
    
    
    IBOutlet UILabel *lbl_friendsCount;
    
    
}

@property(retain,nonatomic)NSMutableArray *arrayFriends;
@property(retain,nonatomic)NSMutableDictionary *registerData;
@end
