//
//  ForgotPasswordViewController.h
//  Attnd
//
//  Created by Jatin Harish on 10/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "Constant.h"
#import "MBProgressHUD.h"
#import "Common.h"


@interface ForgotPasswordViewController : UIViewController
{
    
    IBOutlet UITextField *txtEmail;
    
    IBOutlet UILabel *lbl_EmailError;
    
}
@end
