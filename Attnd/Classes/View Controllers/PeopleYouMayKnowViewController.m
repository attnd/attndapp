//
//  PeopleYouMayKnowViewController.m
//  Attnd
//
//  Created by Jatin Harish on 11/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import "PeopleYouMayKnowViewController.h"


@interface PeopleYouMayKnowViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,WebAPIDelegate>
{
    NSMutableArray *arrayCheckbox;
    NSMutableArray *arrayWithfbID;
    CallAPIClass *objAPI;
    
}

@end

@implementation PeopleYouMayKnowViewController

@synthesize  arrayFriends;
@synthesize registerData;
-(void)viewWillAppear:(BOOL)animated
{
    
}
//-(void)viewWillDisappear:(BOOL)animated
//{
//    [self.navigationController setNavigationBarHidden:YES animated:NO];
//    self.navigationItem.hidesBackButton = YES;
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrayCheckbox = [[NSMutableArray alloc]init];
    arrayWithfbID = [[NSMutableArray alloc]init];
    
    objAPI = [[CallAPIClass alloc]init];
    objAPI.delegate = self;
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationItem.hidesBackButton = YES;
   
    
    
    //set Validation Alert Corner Radius
    lbl_AlertMessage.layer.cornerRadius = 5.0;
    view_ValidationAlert.hidden = YES;
    lbl_ValidationAlert.layer.cornerRadius = 5.0;
    lbl_ValidationAlert.layer.masksToBounds = YES;
    
}
#pragma mark - Button Action

- (IBAction)btnAction_ValidationAlert:(UIButton *)sender
{
    view_ValidationAlert.hidden = YES;
}
- (IBAction)btnActionDone:(UIButton *)sender
{
    if(arrayFriends.count == 0)
    {
        [self hitLoginAPI];
    }
    if (arrayFriends.count <5)
    {
        [self hitLoginAPI];

    }
    else
    {
        
        if (arrayWithfbID.count < 5)
        {
            view_ValidationAlert.hidden = NO;
        }
        else
        {
            [self hitLoginAPI];
            
           
            //        TabBarViewController *contorller = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
            //        [self.navigationController pushViewController:contorller animated:YES];
        }
    }
    
  
}

#pragma mark - API Method
-(void)hitLoginAPI
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    NSMutableDictionary *loginParam = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",[registerData valueForKey:@"email"]], @"email", [NSString stringWithFormat:@"%@",[registerData valueForKey:@"password"]], @"password",nil];
    
    
    [objAPI callServerAPI:loginParam ServiceType:@"POST" ServiceURL:LOGIN];
    
}
-(void)responseServerData:(NSDictionary *)responcedict
{
    NSLog(@"response: %@",responcedict);
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"success"])
    {
        NSDictionary *apiResult = [responcedict valueForKey:@"resData"];
        
        //*-*-*- Save the user token
        [DEFAULTS setValue:[apiResult valueForKey:@"token"] forKey:@"Securitytoken"];
        //*-*-*- Save the user ID
        [DEFAULTS setValue:[[apiResult valueForKey:@"user"] valueForKey:@"_id"] forKey:@"userid"];
      
        NSString *strfacebookID;
        if (arrayFriends.count == 0)
        {
            strfacebookID = @"";
        }
        else
        {
          strfacebookID = [arrayWithfbID componentsJoinedByString:@","];
        }
        
        NSMutableDictionary *addFriendParam = [NSMutableDictionary dictionaryWithObjectsAndKeys:strfacebookID, @"facebookIds",nil];
        
      
        [objAPI callServerAPIwithHeader:addFriendParam ServiceType:@"POST" ServiceURL:ADDFRIEND AutherisationToken:[NSString stringWithFormat:@"BearerAttnd %@",SecurityToken]];
    
        
    }
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"error"])
    {
        
        
    }
    else
    {
        
    }
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}
-(void)responseServerDataWithHeader:(NSDictionary *)responcedict
{
    NSLog(@"response: %@",responcedict);
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"success"])
    {
        TabBarViewController *contorller = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.tabBarController = contorller;
        [self.navigationController pushViewController:contorller animated:YES];
        
        
    }
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"error"])
    {
        
        
    }
    else
    {
        
    }
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    
    
}


#pragma mark - Collection View Delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
{
    return arrayFriends.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    PeopleYouMayKnowCollViewCell *cell = (PeopleYouMayKnowCollViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"FBfriends" forIndexPath:indexPath];
    
    
    
    //*-**-*-*-* Set Data o Labels
    cell.lbl_friendName.text = [[arrayFriends valueForKey:@"name"] objectAtIndex:indexPath.row];
    
    cell.imgView_FriendDP.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", [[arrayFriends valueForKey:@"id"] objectAtIndex:indexPath.row]]]]];
;
    
    //*-*-*-*-* Set Corner Radius to Image View
    cell.imgView_FriendDP.layer.cornerRadius=cell.imgView_FriendDP.frame.size.height/2;
    cell.imgView_FriendDP.layer.masksToBounds = YES;
    cell.imgView_TickMark.layer.cornerRadius=cell.imgView_TickMark.frame.size.height/2;
    cell.imgView_TickMark.layer.masksToBounds = YES;
    
    
    [cell.btnAddFriend addTarget:self action:@selector(ischecked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if ([arrayCheckbox containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]])
    {
        cell.imgView_TickMark.image=  [UIImage imageNamed:@"ic_tick"];
    }
    else
    {
        cell.imgView_TickMark.image=  [UIImage imageNamed:@"Plus"];
        
    }

    cell.btnAddFriend.tag = indexPath.row;
    
    
    
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/3 -10 , 120);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    
   
 
}
#pragma  mark - Button Action

- (IBAction)ischecked:(UIButton *)sender
{
    
    UIButton *btn=(UIButton*)sender;
    
    btn.selected = !btn.selected;
    
    NSIndexPath *clickedButtonIndexPath = [NSIndexPath indexPathForRow:btn.tag inSection:0];
    
    PeopleYouMayKnowCollViewCell  *clicked = [collView_PeopleYouMayKnow cellForItemAtIndexPath:clickedButtonIndexPath];
    
    if (btn.selected)
    {
        clicked.imgView_TickMark.image=  [UIImage imageNamed:@"ic_tick"];
        
        [arrayCheckbox addObject:[NSString stringWithFormat:@"%ld",(long)sender.tag]];
        
        [arrayWithfbID addObject:[[arrayFriends valueForKey:@"id"] objectAtIndex:sender.tag]];
        
    }
    else
        
    {
        clicked.imgView_TickMark.image=  [UIImage imageNamed:@"Plus"];
        
        [arrayCheckbox removeObject:[NSString stringWithFormat:@"%ld",(long)sender.tag]];
        
         [arrayWithfbID removeObject:[[arrayFriends valueForKey:@"id"] objectAtIndex:sender.tag]];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
