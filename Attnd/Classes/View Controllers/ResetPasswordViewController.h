//
//  ResetPasswordViewController.h
//  Attnd
//
//  Created by Jatin Harish on 25/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResetPasswordViewController : UIViewController

{
    
    IBOutlet UILabel *lbl_NewPaswordError;
    
    IBOutlet UITextField *txt_NewPassword;
    
    IBOutlet UILabel *lbl_ConPaswordError;
    
    IBOutlet UITextField *txt_ConPassword;
    
}
@property (retain,nonatomic)NSString *forgotEmail;
@end
