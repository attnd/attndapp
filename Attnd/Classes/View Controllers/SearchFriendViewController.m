//
//  SearchFriendViewController.m
//  Attnd
//
//  Created by Jatin Harish on 12/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import "SearchFriendViewController.h"
#import "SearchFriendCollectionViewCell.h"
#import "Constant.h"
#import "Common.h"

@interface SearchFriendViewController ()
{
    Common *objCommon;
    NSMutableArray *arrayfriends;
}

@end

@implementation SearchFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    objCommon = [Common getInstance];
    arrayfriends = [[NSMutableArray alloc]init];
    arrayfriends = objCommon.facebookFriends;
    
    if (!arrayfriends)
    {
        lbl_NoFriends.hidden = NO;
        lbl_NoFriends.text = @"No Friends";
    }
    
   //    arrayPhotos = [[NSMutableArray alloc]initWithObjects:@"1.jpeg",@"2.jpeg",@"3.jpeg",@"4.jpeg",@"5.jpeg", nil];
//    
//    array_friends = [[NSMutableArray alloc] initWithObjects:@"Jack",@"Harry",@"John",@"Martin",@"Morgan",nil];
   
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
}
#pragma mark - Collection View Delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
{
    return arrayfriends.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    SearchFriendCollectionViewCell *cell = (SearchFriendCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"SearchFriend" forIndexPath:indexPath];
    
    
   
    
    cell.lbl_friendName.text = [[arrayfriends objectAtIndex:indexPath.row] valueForKey:@"name"];
    
    
    cell.imgView_FriendDP.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", [[arrayfriends valueForKey:@"id"] objectAtIndex:indexPath.row]]]]];
    
    
    cell.imgView_FriendDP.layer.cornerRadius=cell.imgView_FriendDP.frame.size.height/2;
    cell.imgView_FriendDP.layer.masksToBounds = YES;
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/4 -10 , 120);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    
}

#pragma mark - Button Action
- (IBAction)btnActionBack:(UIBarButtonItem *)sender
{
    NavigationBack
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
