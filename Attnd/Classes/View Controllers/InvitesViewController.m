//
//  InvitesViewController.m
//  Attnd
//
//  Created by Jatin Harish on 27/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import "InvitesViewController.h"
#import "InvitesTableViewCell.h"
#import "CallAPIClass.h"
#import "Constant.h"
#import "MBProgressHUD.h"

@interface InvitesViewController ()<WebAPIDelegate>
{
    CallAPIClass *objAPI;
}

@end

@implementation InvitesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    objAPI = [[CallAPIClass alloc]init];
    objAPI.delegate = self;
   
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [self getInviteData];
}


#pragma mark - UITable View Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10 ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    invites
//    invite1
    NSString *CellIdentifier;
    
    if (indexPath.row == 0) {
        CellIdentifier = @"invites";
        
    }
    else
    {
        CellIdentifier = @"invite1";
    }
    
    InvitesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[InvitesTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.imgView_Invites.layer.cornerRadius=cell.imgView_Invites.frame.size.height/2;
    cell.imgView_Invites.layer.masksToBounds = YES;
    
    cell.lbl_Username.text = @"Jatin Harish";
    cell.lbl_Description.text = @"Wants to be friend";
    //cell.imgView_EventPic.backgroundColor = [UIColor blueColor];

    cell.btnAcceptedorRejected.layer.cornerRadius = 5.0;
    cell.btnAcceptedorRejected.layer.masksToBounds = YES;
    
    
    
    
    
    
    
    return cell;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    
}
-(void)getInviteData
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strConnectionURL = [NSString stringWithFormat:@"%@",NOTIFICATIONS];
    
    NSString *authorizationToken = [NSString stringWithFormat:@"BearerAttnd %@",SecurityToken];
    
    
    
    [objAPI callServerAPIwithHeader:nil ServiceType:@"GET" ServiceURL:strConnectionURL AutherisationToken:authorizationToken];
}
-(void)responseServerDataWithHeader:(NSDictionary *)responcedict
{
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"success"])
    {
        
    }
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"error"])
    {
        
    }

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
