

//
//  FriendsListViewController.m
//  Attnd
//
//  Created by Gobinder Singh Sidhu on 4/14/17.
//  Copyright © 2017 Gobinder Sidhu. All rights reserved.
//

#import "FriendsListViewController.h"
#import "Constant.h"
#import "Common.h"

@interface FriendsListViewController ()
{
    NSMutableArray *arrayfriends;
    Common *objCommon;
}

@end

@implementation FriendsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrayfiltered = [[NSMutableArray alloc]init];
    
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    objCommon = [Common getInstance];
    arrayfriends = [[NSMutableArray alloc]init];
    arrayfriends = objCommon.facebookFriends;
    
    
    
}

#pragma mark - Search bar delegates
- (void)searchBar:(UISearchBar *) searchBar textDidChange:(NSString *)searchText
{
    [arrayfiltered removeAllObjects];
    if (searchBar.text && [searchBar.text length])
    {
        searchActive = YES;
        
        NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchText];
        
        NSArray *tempfilterArray = [arrayfriends filteredArrayUsingPredicate:filterPredicate];
        NSLog(@"array %@",tempfilterArray);
        arrayfiltered = [tempfilterArray mutableCopy];
        
        NSLog(@"array %@",arrayfiltered);
        
    }
    else
    {
        searchActive = NO;
        //filterdarray = friendsName;
    }
    
    [_collView_friendList reloadData];
}

#pragma mark - Button Action
- (IBAction)btnActionBack:(UIBarButtonItem *)sender
{
    NavigationBack
    
}
#pragma mark - Collection View Delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
{
    if (searchActive)
    {
        return arrayfiltered.count;
    }
    else
    {
        return arrayfriends.count;
    }
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *identifier = @"FriendsCollectionViewCell";
    FriendsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    
    
    
    
    // shadow
    cell.imgView_friendsPic.layer.shadowColor = [UIColor grayColor].CGColor;
    cell.imgView_friendsPic.layer.shadowOffset = CGSizeMake(0, 2);
    cell.imgView_friendsPic.layer.shadowOpacity = 1;
    cell.imgView_friendsPic.layer.shadowRadius = 1.0;
    cell.imgView_friendsPic.clipsToBounds = NO;
    
    [[cell.imgView_friendsPic layer] setCornerRadius:cell.imgView_friendsPic.frame.size.width/2];
    [[cell.imgView_friendsPic layer] setMasksToBounds:YES];
    if (searchActive)
    {
        
        cell.label_friendsName.text = [[arrayfiltered objectAtIndex:indexPath.row] valueForKey:@"name"];
        
        
        cell.imgView_friendsPic.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", [[arrayfiltered valueForKey:@"id"] objectAtIndex:indexPath.row]]]]];    }
    else
        
    {        cell.label_friendsName.text = [[arrayfriends objectAtIndex:indexPath.row] valueForKey:@"name"];
        
        
        cell.imgView_friendsPic.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", [[arrayfriends valueForKey:@"id"] objectAtIndex:indexPath.row]]]]];
    }
    
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/4 -10 , 120);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    
}
//#pragma mark- ------------------Collection View Data source------------------
//
//- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
//
//    return 1;
//}
//
//- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
//
//    return friendsListArray.count;
//}
//
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//
//    CGFloat viewSize = (self.view.bounds.size.width)/3-30;
//    return CGSizeMake(viewSize, viewSize+21);
//}

//-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
//
//    static NSString *identifier = @"FriendsCollectionViewCell";
//    FriendsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
//
//    cell.imgView_friendsPic.image = [UIImage imageNamed:@""];
//    [cell.imgView_friendsPic setBackgroundColor:[UIColor greenColor]];
//
//    [[cell.imgView_friendsPic layer] setCornerRadius:cell.imgView_friendsPic.frame.size.width/2];
//    [[cell.imgView_friendsPic layer] setMasksToBounds:YES];
//
//    // shadow
//    cell.imgView_friendsPic.layer.shadowColor = [UIColor grayColor].CGColor;
//    cell.imgView_friendsPic.layer.shadowOffset = CGSizeMake(0, 2);
//   cell.imgView_friendsPic.layer.shadowOpacity = 1;
//    cell.imgView_friendsPic.layer.shadowRadius = 1.0;
//   cell.imgView_friendsPic.clipsToBounds = NO;
//
//    cell.label_friendsName.text = [ friendsListArray objectAtIndex:indexPath.row ];
//
//
//    return cell;
//
//}
//
//- (IBAction)btn_action_backViewController:(id)sender {
//
//    [self.navigationController popViewControllerAnimated:YES];
//}


//- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText;
//{
//
//    NSString *stringToSearch = searchText;
//
//    if (stringToSearch.length == 0)
//    {
//
//        [friendsListArray removeAllObjects];
//
//        [friendsListArray addObjectsFromArray:backupSearchArray];
//
//
//    }
//
//    else
//    {
//
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
//
//        NSArray *results = [backupSearchArray filteredArrayUsingPredicate:predicate];
//        [friendsListArray removeAllObjects];
//
//        [friendsListArray addObjectsFromArray:results];
//    }
//    [_collectionView_friendList reloadData];
//
//}
//
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
