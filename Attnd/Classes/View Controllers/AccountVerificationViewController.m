//
//  AccountVerificationViewController.m
//  Attnd
//
//  Created by Jatin Harish on 17/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#define forwardTextfieldColor [UIColor colorWithRed:0.7961 green:0.8471 blue:0.9451 alpha:1.0].CGColor;
#define backwardTextfieldColor [UIColor colorWithRed:0.5412 green:0.6235 blue:0.7804 alpha:1.0].CGColor;
#import "AccountVerificationViewController.h"
#import "LastStepViewController.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "Constant.h"
#import "Common.h"
#import "ResetPasswordViewController.h"
#import "CallAPIClass.h"

@interface AccountVerificationViewController ()<WebAPIDelegate>
{
    CallAPIClass *objAPI;
}

@end

@implementation AccountVerificationViewController
@synthesize forgotEmail;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    objAPI = [[CallAPIClass alloc] init];
    objAPI.delegate = self;
    
//    txt_field1.tag  = 1;
//    txt_field2.tag  = 2;
//    txt_field3.tag  = 3;
//    txt_field4.tag  = 4;
    
    [txt_field1 becomeFirstResponder];
    
    txt_field1.textColor = [UIColor whiteColor];
    txt_field2.textColor = [UIColor whiteColor];
    txt_field3.textColor = [UIColor whiteColor];
    txt_field4.textColor = [UIColor whiteColor];
    
    
    txt_field1.layer.cornerRadius = 10.0;
    txt_field1.layer.masksToBounds = YES;
    txt_field1.layer.borderWidth = 4.0;
    txt_field1.layer.borderColor= [UIColor colorWithRed:0.5412 green:0.6235 blue:0.7804 alpha:1.0].CGColor;
    
    txt_field2.layer.cornerRadius = 10.0;
    txt_field2.layer.masksToBounds = YES;
    txt_field2.layer.borderWidth = 4.0;
    txt_field2.layer.borderColor= [UIColor colorWithRed:0.5412 green:0.6235 blue:0.7804 alpha:1.0].CGColor;
    
    txt_field3.layer.cornerRadius = 10.0;
    txt_field3.layer.masksToBounds = YES;
    txt_field3.layer.borderWidth = 4.0;
    txt_field3.layer.borderColor= [UIColor colorWithRed:0.5412 green:0.6235 blue:0.7804 alpha:1.0].CGColor;
    
    txt_field4.layer.cornerRadius = 10.0;
    txt_field4.layer.masksToBounds = YES;
    txt_field4.layer.borderWidth = 4.0;
    txt_field4.layer.borderColor= [UIColor colorWithRed:0.5412 green:0.6235 blue:0.7804 alpha:1.0].CGColor;
}
- (IBAction)btnActionVerify:(UIButton *)sender
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *finalcode = [NSString stringWithFormat:@"%@%@%@%@",txt_field1.text,txt_field2.text,txt_field3.text,txt_field4.text];
    
    
    NSMutableDictionary *VerifyParam = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",finalcode], @"otp", [NSString stringWithFormat:@"%@",forgotEmail], @"email",nil];
    
    
    [objAPI callServerAPI:VerifyParam ServiceType:@"POST" ServiceURL:VERIFYACCOUNT];
    

}
#pragma  mark - API Responce Method
-(void)responseServerData:(NSDictionary *)responcedict

{
    NSLog(@"response: %@",responcedict);
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"success"])
    {
        ResetPasswordViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ResetPasswordViewController"];
        
        controller.forgotEmail = [NSString stringWithFormat:@"%@",forgotEmail];
        [self.navigationController pushViewController:controller animated:YES];
    }
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"error"])
    {
        
        [[Common sharedCommon] showAlertWithOkButton:@"" message:[NSString stringWithFormat:@"%@",[responcedict valueForKey:@"resMessage"]]];
    }
    else
    {
        
    }
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ((textField.text.length < 1) && (string.length > 0))
    {
        if (textField == txt_field1)
        {
            txt_field1.layer.borderColor= forwardTextfieldColor
            [txt_field2 becomeFirstResponder];
        }
        if(textField == txt_field2)
        {
            txt_field2.layer.borderColor= forwardTextfieldColor
            [txt_field3 becomeFirstResponder];
        }
        if(textField == txt_field3)
        {
            txt_field3.layer.borderColor= forwardTextfieldColor
            [txt_field4 becomeFirstResponder];
        }
        if (textField == txt_field4)
        {
            txt_field4.layer.borderColor= forwardTextfieldColor
        }
        
        textField.text = string;
        return false;
    }
    //On Deleting
    else if((textField.text.length >= 1) && (string.length == 0))
    {
        if (textField == txt_field2)
        {
            [txt_field1 becomeFirstResponder];
            txt_field2.layer.borderColor= backwardTextfieldColor
        }
        if (textField == txt_field3)
        {
            txt_field3.layer.borderColor= backwardTextfieldColor
            
            [txt_field2 becomeFirstResponder];
        }
        if (textField == txt_field4)
        {
            txt_field4.layer.borderColor= backwardTextfieldColor
            [txt_field3 becomeFirstResponder];
        }
        if (textField == txt_field1)
        {
            txt_field1.layer.borderColor= backwardTextfieldColor
        }
        textField.text=@"";
        return false;
    }
    else if(textField.text.length >= 1)
    {
        textField.text = string;
        return false;
    }
    return true;
    
    
}

//#pragma mark - Textfield Delegates
//
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    //    if (textField.tag ==1)
//    //    {
//    //        [txt_field2 becomeFirstResponder];
//    //    }
//    //
//    //    else if (textField.tag ==2)
//    //    {
//    //        [txt_field3 becomeFirstResponder];
//    //    }
//    //    else if (textField.tag ==3)
//    //    {
//    //        [txt_field4 becomeFirstResponder];
//    //    }
//    //    else if (textField.tag ==4)
//    //    {
//    //        [txt_field1 becomeFirstResponder];
//    //    }
//    //    else
//    //    {
//    //
//    //    }
//    //    return YES;
//    if  (textField.text.length == 1)
//    {
//        textField.layer.borderColor= [UIColor redColor].CGColor;
//        
//        nextTag = textField.tag + 1;
//        // Try to find next responder
//        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
//        if (! nextResponder)
//            nextResponder = [textField.superview viewWithTag:1];
//        
//        
//        if (nextResponder)
//            //Found next responder, so set it.
//            [nextResponder becomeFirstResponder];
//        
//        
//        return YES;
//    }
//    else if (string.length > 0)
//    {
//        
//        
//        textField.layer.borderColor= [UIColor redColor].CGColor;
//        
//        nextTag = textField.tag + 1;
//        // Try to find next responder
//        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
//        //  if (! nextResponder)
//        // nextResponder = [textField.superview viewWithTag:1];
//        
//        
//        //        if (nextResponder)
//        // Found next responder, so set it.
//        // [nextResponder becomeFirstResponder];
//        
//        
//        return YES;
//        
//    }
//    else
//    {
//        return NO;
//    }
//    
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
