//
//  EventDetailViewController.h
//  Attnd
//
//  Created by Jatin Harish on 13/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventDetailViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

{
    IBOutlet UITableView *tblView_EventDetail;


    IBOutlet UIButton *btnInvite, *btncheckIn;
    
    NSMutableArray *array_Hosts,*array_Highlights;

    IBOutlet UIView *view_Bottom;
}
@property (retain, nonatomic) NSString *isComeFromAddEvent;
@end
