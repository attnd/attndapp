//
//  EventDetailViewController.m
//  Attnd
//
//  Created by Jatin Harish on 13/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import "EventDetailViewController.h"
#import "EventdetailTableViewCell.h"
#import "SearchFriendCollectionViewCell.h"
#import "InviteFriendsViewController.h"
#import "WhoGoingViewController.h"
#import "EditEventViewController.h"
#import "Constant.h"



@interface EventDetailViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@end

@implementation EventDetailViewController
@synthesize isComeFromAddEvent;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //*-*-*-*-*-*- Shadow for bottom View
    CALayer *layer = view_Bottom.layer;
    layer.shadowOffset = CGSizeMake(1, 1);
    layer.shadowColor = [[UIColor blackColor] CGColor];
    layer.shadowRadius = 4.0f;
    layer.shadowOpacity = 0.80f;
    layer.shadowPath = [[UIBezierPath bezierPathWithRect:layer.bounds] CGPath];
    
    
    array_Hosts = [[NSMutableArray alloc] init];
    array_Hosts= [NSMutableArray arrayWithObjects:@"1",@"2",@"3", nil];
    
    array_Highlights = [[NSMutableArray alloc] init];
    array_Highlights = [NSMutableArray arrayWithObjects:@"1", nil];
    
    tblView_EventDetail.rowHeight = UITableViewAutomaticDimension;
    tblView_EventDetail.estimatedRowHeight = 140;
    
    //Set buttons Corner Radius
    btnInvite.layer.cornerRadius = 5.0;
    btnInvite.layer.masksToBounds = YES;
    btncheckIn.layer.cornerRadius = 5.0;
    btncheckIn.layer.masksToBounds = YES;
    
    //*-*-*-*-* Shadow to Bottom View
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}
//-(void)viewDidDisappear:(BOOL)animated
//{
//    [self.navigationController setNavigationBarHidden:YES animated:NO];
//    
//}
#pragma mark - UITable View Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //uppercell
    //
    static NSString *CellIdentifier;
    if (indexPath.row == 0)
    {
        CellIdentifier = @"event";
    }
    if (indexPath.row == 1)
    {
        CellIdentifier =@"Whogoing";
    }
    if (indexPath.row == 2)
    {
        CellIdentifier =@"tellfriend";
    }
    if (indexPath.row == 3)
    {
        CellIdentifier = @"Highlights";
    }
    if (indexPath.row == 4)
    {
        CellIdentifier = @"Hosts";
    }
    
    EventdetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[EventdetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    //    *-*-*-*-*-*-*-*-*-*-*-* for background menu *-*-*-*-*-*-*-*-*-*-*-*
    cell.viewBackgroundMain.layer.borderWidth=0.5;
    cell.viewBackgroundMain.layer.borderColor=[[UIColor colorWithWhite:0.8f alpha:1.5f] CGColor];
    cell.viewBackgroundMain.layer.masksToBounds = NO;
    cell.viewBackgroundMain.layer.cornerRadius = 2; // if you like rounded corners
    cell.viewBackgroundMain.layer.shadowColor=[[UIColor darkGrayColor] CGColor];
    cell.viewBackgroundMain.layer.shadowOffset = CGSizeMake(0, 1);
    
    //*-*-*-*-*-*-*-*-*-*-*-* For event detail cell *-*-*-*-*-*-*-* //
    
    cell.imgView_EventPic.layer.cornerRadius = 5.0;
    cell.imgView_EventPic.clipsToBounds = YES;
    
    // *-*-*-*-*-*-**-*-*-*-*- For tell A Friend Cell *-*-*-**-*-*-*//
    
    
    [cell.btnSnapChat addTarget:self action:@selector(btnActionViewAll) forControlEvents:UIControlEventTouchUpInside];
    // *-*-*-*-*-*-**-*-*-*-*- For Who;s Going Cell *-*-*-**-*-*-*//
    
    [cell.btnViewAllWhosGoing addTarget:self action:@selector(btnActionViewAll) forControlEvents:UIControlEventTouchUpInside];
    
    //*-*-*-*-*-*-**-*-*-*-**- For Diffrenciate between Collection View
    cell.collView_WhosGoing.tag = 01;
    cell.collView_Hosts.tag = 02;
    cell.collView_Highlights.tag = 03;
    [cell.collView_Highlights reloadData];
    
    
    
    
    
    return cell;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{

    
}
#pragma mark - Collection View Delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
{
    if(collectionView.tag == 01)
    {
        return 10;
    }
    if (collectionView.tag == 02)
    {
        return array_Hosts.count+1;
    }
    if (collectionView.tag == 03)
    {
        return array_Highlights.count;
    }
    else
    {
        return 0;
    }
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    SearchFriendCollectionViewCell *cell = (SearchFriendCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"SearchFriend" forIndexPath:indexPath];
    
    
    //    cell.imgView_SelectCoins.image
    
    cell.lbl_friendName.text = @"Jatin Harish";
    
    if (collectionView.tag  == 01)
    {
        //cell.imgView_FriendDP.backgroundColor = [UIColor redColor];
        cell.imgView_FriendDP.layer.cornerRadius=cell.imgView_FriendDP.frame.size.height/2;
        cell.imgView_FriendDP.layer.masksToBounds = YES;
    }
    if (collectionView.tag  == 02)
    {
        cell.imgView_FriendDP.layer.cornerRadius=cell.imgView_FriendDP.frame.size.height/2;
        cell.imgView_FriendDP.layer.masksToBounds = YES;
        if (indexPath.row == array_Hosts.count)
        {
            cell.imgView_FriendDP.image = [UIImage imageNamed:@"message"];
            cell.lbl_friendName.text = @"Message";
        }
        else
            
        {
            cell.imgView_FriendDP.image = [UIImage imageNamed:@"image_Profile"];
            
        }
        
    }
    
    if (collectionView.tag  == 03)
    {
        cell.imgView_FriendDP.layer.cornerRadius = 5.0;
        cell.imgView_FriendDP.layer.masksToBounds = YES;
        if(indexPath.row == 0)
        {
            cell.imgView_FriendDP.image = [UIImage imageNamed:@"add_highlight"];
            cell.lbl_friendName.text = @"ADD HIGHLIGHT";
            [cell.lbl_friendName setFont:[UIFont fontWithName:@"VarelaRound" size:9]];
            
        }
        else
        {
            cell.imgView_FriendDP.image = [array_Highlights objectAtIndex:indexPath.row];
            
            cell.lbl_friendName.text = @"";
        }
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag == 03)
    {
        if (indexPath.row == 0)
        {
            return CGSizeMake(self.view.frame.size.width/4-5, 140);
        }
        else
        {
            return CGSizeMake(self.view.frame.size.width/4-5, 100);
        }
        
    }
    else
    {
        return CGSizeMake(self.view.frame.size.width/5, 80);
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    if (collectionView.tag == 03)
    {
        if (indexPath.row == 0)
        {
            UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"Add Highlights" preferredStyle:UIAlertControllerStyleActionSheet];
            
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                [self dismissViewControllerAnimated:YES completion:^{
                }];
            }]];
            
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"From Camera" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                            // Distructive button tapped.
                
                [self openCamera];
                
            }]];
            
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"From Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self openGallery];
               
            }]];
            
            // Present action sheet.
            [self presentViewController:actionSheet animated:YES completion:nil];
        }
        else
        {
            
            
        }
        
    }

}
-(void)openCamera
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:NULL];
}
-(void)openGallery
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}

#pragma mark - Image Picker Delegates

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [array_Highlights addObject:chosenImage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    [tblView_EventDetail reloadData];
    
}

#pragma mark - Button Action
- (IBAction)btnActionBack:(UIBarButtonItem *)sender
{
    //NavigationBack
    UINavigationController *nav = (UINavigationController *)self.parentViewController;
    [nav popViewControllerAnimated:YES];
}

- (IBAction)btnActionmenu:(UIBarButtonItem *)sender
{
    EditEventViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"EditEventViewController"];
    
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
}





-(void)btnActionViewAll
{
    WhoGoingViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WhoGoingViewController"];
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
}
- (IBAction)btnActionInvite:(UIButton *)sender
{
    InviteFriendsViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"InviteFriendsViewController"];
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
