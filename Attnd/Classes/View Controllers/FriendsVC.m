//
//  FriendsVC.m
//  Attnd
//
//  Created by Gobinder Singh Sidhu on 4/11/17.
//  Copyright © 2017 Gobinder Sidhu. All rights reserved.
//

#import "FriendsVC.h"
#import "SearchFriendViewController.h"
#import "Common.h"
#import "Constant.h"
@interface FriendsVC ()
{
    
    NSMutableArray *arrayfriends;
}

@end

@implementation FriendsVC
@synthesize arrayFriends;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if (arrayFriends.count == 0)
    {
        lbl_Nofriends.hidden = NO;
        lbl_Nofriends.text = @"No Friends";
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    
   [_collectionView_friends reloadData];
    
}

#pragma mark- ------------------Collection View Data source------------------

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return arrayFriends.count;
}



- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/3 -10 , 133);
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"profileFriendsCell";
    FriendsProfileCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    // cell.imgView_FriendPic.image = [UIImage imageNamed:@"samplePic"];
   
    
    cell.label_FriendName.text = [[arrayFriends objectAtIndex:indexPath.row] valueForKey:@"realName"];
    
    
    //cell.imgView_FriendPic.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[arrayFriends objectAtIndex:indexPath.row] valueForKey:@"profilePic"]]]]];
    
    
    [cell.label_FriendName setTextColor:[UIColor blackColor]];
    
    // shadow
    cell.imgView_FriendPic.layer.shadowColor = [UIColor grayColor].CGColor;
    cell.imgView_FriendPic.layer.shadowOffset = CGSizeMake(0, 2);
    cell.imgView_FriendPic.layer.shadowOpacity = 1;
    cell.imgView_FriendPic.layer.shadowRadius = 1.0;
    cell.imgView_FriendPic.clipsToBounds = NO;
    
    
    [[cell.imgView_FriendPic layer] setCornerRadius:cell.imgView_FriendPic.frame.size.width/2];
    [[cell.imgView_FriendPic layer] setMasksToBounds:YES];
    
    return cell;
    
}


- (IBAction)btn_action_viewall:(id)sender {
    
    
//    UINavigationController *nav = (UINavigationController *)self.parentViewController.view.window.rootViewController;
//    
//    SearchFriendViewController *contorller = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchFriendViewController"];
//    
//    [nav pushViewController:contorller animated:YES];
    UINavigationController *nav = (UINavigationController *)self.parentViewController.view.window.rootViewController;
    
    
    FriendsListViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendsListViewController"];
    
    [nav pushViewController:controller animated:YES];
    
    
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
//    
//     UINavigationController *navCon = (UINavigationController *)self.parentViewController.view.window.rootViewController;
//    
//    ProfileVC *controller1 = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileVC"];
//
//   // controller1.navigateTo = @"3";
//    
//    [navCon pushViewController:controller1 animated:YES];
//    
//    //  [self.navigationController pushViewController:controller1 animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
