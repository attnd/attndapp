//
//  PhotosVC.m
//  Attnd
//
//  Created by Gobinder Singh Sidhu on 4/11/17.
//  Copyright © 2017 Gobinder Sidhu. All rights reserved.
//

#import "PhotosVC.h"
#import "Constant.h"

@interface PhotosVC ()

@end

@implementation PhotosVC
@synthesize arrayUserPhotos;

- (void)viewDidLoad {
    [super viewDidLoad];
    
  
    
}

-(void)viewWillAppear:(BOOL)animated
{
    if (arrayUserPhotos.count == 0)
    {
        lbl_NoPhotos.hidden = NO;
        lbl_NoPhotos.text = @"No Photos";
    }

    [_collectionView_photos reloadData];

}

#pragma mark- ------------------Collection View Data source------------------

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return arrayUserPhotos.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/3 -10 , 120);
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"photoCell";
    PhotosCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.imgView_photos.layer.cornerRadius = 5.0;
    //*-*-*-*-*-**-*-* shadow
     cell.imgView_photos.layer.shadowColor = [UIColor grayColor].CGColor;
     cell.imgView_photos.layer.shadowOffset = CGSizeMake(0, 2);
     cell.imgView_photos.layer.shadowOpacity = 1;
     cell.imgView_photos.layer.shadowRadius = 1.0;
     cell.imgView_photos.clipsToBounds = NO;
    
    cell.btn_deletePhoto.hidden=NO;
    
    cell.btn_deletePhoto.tag = indexPath.row;
    
    [cell.btn_deletePhoto addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    //*-*-*-*-* Set data on labels *-*-*-*-*-*-*-*
    
    cell.imgView_photos.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kImgBaseURL,[[arrayUserPhotos objectAtIndex:indexPath.row] valueForKey:@"file"]]]]];
    
    
    return cell;
    
}

-(void)yourButtonClicked:(UIButton*)sender
{
    //[photosArr removeObjectAtIndex:sender.tag];
    
    //[_collectionView_photos reloadData];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
