//
//  EventsViewController.m
//  Attnd
//
//  Created by Jatin Harish on 12/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import "EventsViewController.h"
#import "EventsTableViewCell.h"
#import "EventsCollectionViewCell.h"
#import "EventDetailViewController.h"
#import "Constant.h"

@interface EventsViewController ()

@end

@implementation EventsViewController
@synthesize arrayEvents;
//@synthesize homeTab;
-(void)viewWillAppear:(BOOL)animated
{
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (arrayEvents.count == 0)
    {
        lbl_NoEvents.hidden = NO;
        lbl_NoEvents.text = @"No Events";
    }
    
    //self.homeTab = [[HomeViewController alloc] init];
}
#pragma mark - UITable View Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (arrayEvents)
    {
        return arrayEvents.count;
    }
    else
    {
        return 4;
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //uppercell
    
    static NSString *CellIdentifier = @"event";
    
    EventsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[EventsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
   
    //set background View as Box
    cell.view_Background.layer.borderWidth=0.5;
    cell.view_Background.layer.borderColor=[[UIColor colorWithWhite:0.8f alpha:1.5f] CGColor];
    cell.view_Background.layer.masksToBounds = NO;
    cell.view_Background.layer.cornerRadius = 2; // if you like rounded corners
    cell.view_Background.layer.shadowColor=[[UIColor darkGrayColor] CGColor];
    cell.view_Background.layer.shadowOffset = CGSizeMake(0, 1);
    
    
    cell.imgView_EventPic.layer.cornerRadius = 5.0;
    cell.imgView_EventPic.layer.masksToBounds = YES;
    cell.colView_attndingPeople.tag = indexPath.row;
    
    
    //*-*-*-*-*-* Set Data on Labels
    if (arrayEvents)
    {
        cell.imgView_EventPic.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kImgBaseURL,[[arrayEvents objectAtIndex:indexPath.row] valueForKey:@"eventImg"]]]]];
        
        cell.lbl_EventTitle.text = [NSString stringWithFormat:@"%@",[[arrayEvents objectAtIndex:indexPath.row] valueForKey:@"title"]];
    }
    else
    {
        
    }
    
    
    
    return cell;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
   UINavigationController *nav = (UINavigationController *)self.parentViewController.view.window.rootViewController;
    
    EventDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailViewController"];
    
    [nav pushViewController:controller animated:YES];
    
 }

#pragma mark - Collection View Delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
{
    if(arrayEvents)
    {
        return [[[arrayEvents objectAtIndex:collectionView.tag] valueForKey:@"hosts"] count];
    
    }
    else
    {
        return 23;
    }
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    EventsCollectionViewCell *cell = (EventsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Events" forIndexPath:indexPath];
    
    
    cell.imgView_attendingPerson.layer.cornerRadius = cell.imgView_attendingPerson.frame.size.height/2;
    cell.imgView_attendingPerson.layer.masksToBounds = YES;
    
    // set data on controls
    
    if (arrayEvents)
    {
         cell.imgView_attendingPerson.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kImgBaseURL,[[[[arrayEvents objectAtIndex:collectionView.tag] valueForKey:@"hosts"] objectAtIndex:indexPath.row] valueForKey:@"profilePic"]]]]];
    }
    else
    {
        
    }
   
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/7 , 120);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation



@end
