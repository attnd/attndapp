//
//  HomeViewController.m
//  Attnd
//
//  Created by Jatin Harish on 12/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//
//facebook.samples.FriendSmash
#import "HomeViewController.h"
#import "TapToSeeOutViewController.h"
#import "EventDetailViewController.h"
#import "CAPSPageMenu.h"
#import "ActivityViewController.h"
#import "EventsViewController.h"
#import "SearchFriendViewController.h"
#import "SettingViewController.h"

@interface HomeViewController ()
@property (nonatomic) CAPSPageMenu *pageMenu;

@end

@implementation HomeViewController
-(void)viewWillAppear:(BOOL)animated
{
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //[self.navigationController setNavigationBarHidden:NO animated:NO];
    
   // self.navigationController.navigationBar.topItem.title = @"FRIEND ACTIVITY";
    
    //self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    
    
    //self.navigationController.navigationBar.topItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search"] landscapeImagePhone:nil style:UIBarButtonItemStyleDone target:self action:@selector(didTapSearch)];
    //self.navigationController.navigationBar.topItem.leftBarButtonItem.tintColor = [UIColor blackColor];
    
    
    //self.navigationController.navigationBar.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_Tap"] landscapeImagePhone:nil style:UIBarButtonItemStyleDone target:self action:@selector(didTapTAPButton)];
    //self.navigationController.navigationBar.topItem.rightBarButtonItem.tintColor = [UIColor clearColor];
    
    ActivityViewController *ActivityView = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivityViewController"];
    ActivityView.title = @"ACTIVITY";
    
    EventsViewController *EventView = [self.storyboard instantiateViewControllerWithIdentifier:@"EventsViewController"];
    
    EventView.title = @"EVENTS";
    
    
    
    
    NSArray *controllerArray = @[ActivityView, EventView];
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionSelectionIndicatorColor: [UIColor colorWithRed:0.3333 green:0.5373 blue:0.9725 alpha:1.0],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:70.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"VarelaRound" size:13.0],
                                 CAPSPageMenuOptionMenuHeight: @(30.0),
                                 CAPSPageMenuOptionMenuItemWidth: @(self.view.frame.size.width/2),
                                 CAPSPageMenuOptionCenterMenuItems: @(YES)
                                 };
    
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 64, self.view.frame.size.width, self.view.frame.size.height) options:parameters];
    [self.view addSubview:_pageMenu.view];
    
    
}
#pragma  mark - Button Action
- (IBAction)btnActionSearchTap:(UIBarButtonItem *)sender
{
    UINavigationController *nav = (UINavigationController *)self.parentViewController.view.window.rootViewController;
    
    SearchFriendViewController *contorller = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchFriendViewController"];
    
    [nav pushViewController:contorller animated:YES];
    
}
- (IBAction)btnActionTap:(UIBarButtonItem *)sender
{
    UINavigationController *nav = (UINavigationController *)self.parentViewController.view.window.rootViewController;
    TapToSeeOutViewController  *contorller = [self.storyboard instantiateViewControllerWithIdentifier:@"TapToSeeOutViewController"];
    [nav pushViewController:contorller animated:YES];
    
 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
