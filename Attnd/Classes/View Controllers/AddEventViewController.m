//
//  AddEventViewController.m
//  Attnd
//
//  Created by Jatin Harish on 14/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//
#define BtnSelectedBackground [UIColor colorWithRed:0.3255 green:0.5333 blue:0.9725 alpha:1.0]

#define BtnUnselectedBackground [UIColor colorWithRed:0.8549 green:0.9020 blue:0.9961 alpha:1.0]

#define PlaceHolderColor [UIColor colorWithRed:0.6039 green:0.7333 blue:0.9882 alpha:1.0]

#define validationPlaceHolderColor [UIColor colorWithRed:0.9294 green:0.3647 blue:0.3647 alpha:1.0]

#import "AddEventViewController.h"
#import "Constant.h"
#import "AppDelegate.h"
#import "TabBarViewController.h"
#import "Common.h"
#import "AddHostsViewController.h"
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Constant.h"
#import "CallAPIClass.h"
#import "EventDetailViewController.h"
#import "AddHostsViewController.h"
#import "friendListModal.h"



@interface AddEventViewController ()<UIImagePickerControllerDelegate,WebAPIDelegate,UITextFieldDelegate,addHostDelegate>
{
    Common *objCommon;
    NSMutableArray  *arrayHosts;
    UIActionSheet *popup;
    NSString  *strIsPrivateOrPublic;
    CallAPIClass *objAPI;
    UIDatePicker *datePicker,*timePicker;
    NSString *iswhichTextfield;
    
}


@end

@implementation AddEventViewController

-(void)getHostsArray:(NSMutableArray *)array
{
    
    for (int i = 0; i< array.count; i++)
    {
        friendListModal *objModal = [array objectAtIndex:i];
       // NSString *strID = [NSString stringWithFormat:@"%@",objModal.friend_id];
        [arrayHosts addObject:objModal];
        
    }
    
    NSLog(@"Hosts Array %@",arrayHosts);
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    iswhichTextfield=@"";
    objAPI = [[CallAPIClass alloc]init];
    objAPI.delegate = self;
    
    arrayHosts = [[NSMutableArray alloc]init];
    friendListModal *objModal = [[friendListModal alloc]init];
    objModal.friend_id = UserID;
    objModal.friend_ProfilePic = userProfilePic;
    objModal.friend_name = @"";
    
    [arrayHosts addObject:objModal];
   
    strIsPrivateOrPublic = [NSString stringWithFormat:@"public"];
    
    btnPrivate.backgroundColor = BtnUnselectedBackground;
    btnPublic.backgroundColor = BtnSelectedBackground;
    
    objCommon = [Common getInstance];
    
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    //*-**-*-* Add Shadow to UIView (Bottom view)
    CALayer *layer = view_Bottom.layer;
    layer.shadowOffset = CGSizeMake(1, 1);
    layer.shadowColor = [[UIColor blackColor] CGColor];
    layer.shadowRadius = 4.0f;
    layer.shadowOpacity = 0.80f;
    layer.shadowPath = [[UIBezierPath bezierPathWithRect:layer.bounds] CGPath];
    
    //*-*-*-*-*-*-*-* set corner Radius to Controls
    btnmakeEventLive.layer.cornerRadius = 5.0;
    btnPublic.layer.cornerRadius = 5.0;
    btnPrivate.layer.cornerRadius = 5.0;
    imgView_EventImage.layer.cornerRadius = 5.0;
    imgView_EventImage.layer.masksToBounds = YES;
    btnmakeEventLive.layer.masksToBounds = YES;
    btnPublic.layer.masksToBounds = YES;
    btnPrivate.layer.masksToBounds = YES;
    
    
    
    //*-*-*-*-*-* Tap Gesture on Event Image
    popup = [[UIActionSheet alloc] initWithTitle:@"Set Event Picture:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
             @"From Camera",
             @"From Gallery",
             nil];
    popup.tag = 1;
    //[popup showInView:self.view];
    
    // *-*-*-*-*-*-*-*-*-* Set tap gesture on image view Profile
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction)];
    singleTap.numberOfTapsRequired = 1;
    singleTap.numberOfTouchesRequired = 1;
    singleTap.delegate = self;
    [imgView_EventImage addGestureRecognizer:singleTap];
    imgView_EventImage.userInteractionEnabled = YES;
    
    
    //*-*-*-*--*-**-Set Placeholder Color
    txt_EventTitle.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Name of your event" attributes:@{NSForegroundColorAttributeName: PlaceHolderColor}];
    txt_Address.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Location of your event" attributes:@{NSForegroundColorAttributeName: PlaceHolderColor}];
    txt_Date.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Date of your event" attributes:@{NSForegroundColorAttributeName: PlaceHolderColor}];
    txt_Time.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Time of your event" attributes:@{NSForegroundColorAttributeName: PlaceHolderColor}];
    
    //*-*-*-*-*-*-** Date Picker
   // [self setDateandTimePicker];
    txt_Time.delegate =self;
    txt_Date.delegate = self;
    txt_Address.delegate = self;
    txt_EventTitle.delegate = self;
    
}

#pragma mark -Text field delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;
{
    datePicker=[[UIDatePicker alloc]init];
    
    //*-*-*-*-* For Tool Bar
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    [toolBar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(ShowSelectedDate)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
    
    ///////////////////
    if(textField == txt_Date)
    {
        iswhichTextfield = [NSString stringWithFormat:@"date"];
        datePicker.datePickerMode = UIDatePickerModeDate;
        [txt_Date setInputView:datePicker];
        [txt_Date setInputAccessoryView:toolBar];
    }
    if(textField == txt_Time)
    {
        iswhichTextfield = [NSString stringWithFormat:@"time"];
        datePicker.datePickerMode = UIDatePickerModeTime;
      
        //*-*-*-*-*-*-* Time format for 12 Hours *-*-*-*-*//
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        NSString *usFormatString = [NSDateFormatter dateFormatFromTemplate:@"EdMMM" options:0 locale:usLocale];
        NSLog(@"usFormatterString: %@", usFormatString);
        datePicker.locale = usLocale;
        NSLog(@"US Time format is ==== %@",datePicker.date);
        //*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
        
        
        //*-*-*-*-*-*-* Time format for 24 Hours *-*-*-*-*//
        /*NSLocale *gbLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"];
        NSString *gbFormatString = [NSDateFormatter dateFormatFromTemplate:@"EdMMM" options:0 locale:gbLocale];
        NSLog(@"gbFormatterString: %@", gbFormatString);
        datePicker.locale = gbLocale;
        NSLog(@"GB Time format is ==== %@",datePicker.date);*/
        //*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
        
        [txt_Time setInputView:datePicker];
        [txt_Time setInputAccessoryView:toolBar];


    }
  
    return YES;
    
}
#pragma mark - Cunstom Methods

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

- (NSString *)dateStringFromString:(NSString *)sourceString
                      sourceFormat:(NSString *)sourceFormat
                 destinationFormat:(NSString *)destinationFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:sourceFormat];
    NSDate *date = [dateFormatter dateFromString:sourceString];
    [dateFormatter setDateFormat:destinationFormat];
    return [dateFormatter stringFromDate:date];
}

-(void)ShowSelectedDate
{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    if ([iswhichTextfield isEqualToString:@"date"])
    {
         [formatter setDateFormat:@"dd/MM/yyyy"];
         txt_Date.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:datePicker.date]];
         [txt_Date resignFirstResponder];
    }
    else if ([iswhichTextfield isEqualToString:@"time"])
    {
        
        [formatter setDateFormat:@"hh:mm a"];
        txt_Time.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:datePicker.date]];
        [txt_Time resignFirstResponder];
        
    }
 
}
-(void)viewWillAppear:(BOOL)animated
{
   
}

#pragma mark - Button Action
- (IBAction)btnActionPublic:(UIButton *)sender
{
    btnPublic.backgroundColor = BtnSelectedBackground;
    btnPrivate.backgroundColor = BtnUnselectedBackground;
    strIsPrivateOrPublic = [NSString stringWithFormat:@"public"];
}
- (IBAction)btnActionPrivate:(UIButton *)sender
{
    btnPrivate.backgroundColor = BtnSelectedBackground;
    btnPublic.backgroundColor = BtnUnselectedBackground;
    strIsPrivateOrPublic = [NSString stringWithFormat:@"private"];
}

//#pragma mark ############## BASe 64 Methods ############
//
//-(NSString *)imageToNSString:(UIImage *)image
//{
//    NSData *imageData = UIImagePNGRepresentation(image);
//    return [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
//}
//
//-(UIImage *)stringToUIImage:(NSString *)string
//{
//    NSData *data = [[NSData alloc]initWithBase64EncodedString:string
//                                                      options:NSDataBase64DecodingIgnoreUnknownCharacters];
//    return [UIImage imageWithData:data];
//}


- (IBAction)btnActionMakeEventLive:(UIButton *)sender

{
    if ([self validate])
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSLog(@"userId === %@,SecurityToken === %@",UserID,SecurityToken);
        NSString *strauthorizationToken = [NSString stringWithFormat:@"BearerAttnd %@",SecurityToken];
        
      
        NSArray *arr = [[NSString stringWithFormat:@"%@",txt_Time.text] componentsSeparatedByString:@" "];
        NSString *strTime = [NSString stringWithFormat:@"%@:00 %@",[arr objectAtIndex:0],[arr objectAtIndex:1]];
        
        NSString *strDateTime = [self dateStringFromString:[NSString stringWithFormat:@"%@ %@",txt_Date.text,strTime]  sourceFormat:@"dd/MM/yyyy hh:mm:ss a" destinationFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
        
        
        NSString *strArrayHosts;
        
        for (int i = 0; i< arrayHosts.count; i++)
        {
            friendListModal *objModal = [arrayHosts objectAtIndex:i];
            NSString *strID = [NSString stringWithFormat:@"%@",objModal.friend_id];
            if (strArrayHosts)
            {
                strArrayHosts = [NSString stringWithFormat:@"%@,%@",strArrayHosts,strID];
            }
            else
            {
              strArrayHosts = [NSString stringWithFormat:@"%@",strID];
            }
            

        }

        
        NSMutableDictionary *eventParam = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           txt_EventTitle.text, @"title",
                                           strArrayHosts, @"hosts",
                                           txt_Address.text,@"address",
                                           [NSString stringWithFormat:@"%@",strDateTime], @"dateTime",
                                           strIsPrivateOrPublic,@"type",
                                           nil];
        
        NSData *imgData;
        imgData = [[NSData alloc]init];
        UIImage *image = [UIImage imageNamed:@"add_photo"];
        if ([UIImagePNGRepresentation(imgView_EventImage.image) isEqualToData:UIImagePNGRepresentation(image)])
        {
            NSLog(@"No Image");
        }
        
        else
        {
            imgData = UIImageJPEGRepresentation([self imageWithImage:imgView_EventImage.image convertToSize:CGSizeMake(10, 100)], 0.7);
  
        }
         [objAPI callServerAPIwithMultipart:eventParam ServiceType:@"POST" ServiceURL:ADDEVENT AutherisationToken:strauthorizationToken ImgData:imgData ImgParameter:@"eventImg"];

    }
}


- (IBAction)btnActionBack:(UIBarButtonItem *)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.tabBarController.selectedIndex = [objCommon.fromwhichIndex intValue];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
#pragma mark - API Responce Method
-(void)responseServerData:(NSDictionary *)responcedict
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSLog(@"response: %@",responcedict);
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"success"])
    {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@""
                                                              message:@"Event Added Successfully"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
        EventDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailViewController"];
        [self.navigationController pushViewController:controller animated:YES];
        
        
    }
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"error"])
    {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@""
                                                              message:[responcedict valueForKey:@"resMessage"]
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
    }
    else
    {
        
    }
}

#pragma mark - Collection View Delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
{
    return arrayHosts.count + 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    AddEventCollectionViewCell *cell = (AddEventCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"addEvent" forIndexPath:indexPath];
    
    cell.imgView_HostPic.layer.cornerRadius=cell.imgView_HostPic.frame.size.height/2;
    cell.imgView_HostPic.layer.masksToBounds = YES;
    
    friendListModal *objModal = [arrayHosts objectAtIndex:indexPath.row];
    
    
    if (indexPath.row == 0)
    {
        
        if (objModal.friend_ProfilePic)
        {
             cell.imgView_HostPic.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kImgBaseURL,objModal.friend_ProfilePic]]]];
        }
        else
        {
             cell.imgView_HostPic.image = [UIImage imageNamed:@"defualt.png"];
        }
       
        cell.btnCross.hidden = YES;
    }
    
    if (indexPath.row == 1)
    {
        cell.imgView_HostPic.image = [UIImage imageNamed:@"addhost"];
        cell.imgView_HostPic.backgroundColor = [UIColor clearColor];
        cell.btnCross.hidden = YES;
    }
    
    
    
    
    return cell;
}
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    return CGSizeMake(self.view.frame.size.width/3 -10 , 120);
//}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    
    if (indexPath.row == 1)
    {
        AddHostsViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"AddHostsViewController"];
        controller.hostDelegate = self;
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    
}
-(void)tapGestureAction
{
    [popup showInView:self.view];
    
}
-(BOOL)validate
{
    if (txt_EventTitle.text.length==0)
    {
        txt_EventTitle.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Name of your event" attributes:@{NSForegroundColorAttributeName: validationPlaceHolderColor}];
        
        return false;
    }
    if (txt_Address.text.length==0)
    {
        txt_Address.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Location of your event" attributes:@{NSForegroundColorAttributeName: validationPlaceHolderColor}];
        return false;
    }

    if (txt_Date.text.length==0)
    {
        
        txt_Date.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Date of your event" attributes:@{NSForegroundColorAttributeName: validationPlaceHolderColor}];
       
        return false;
    }
    if (txt_Time.text.length==0)
    {
         txt_Time.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Time of your event" attributes:@{NSForegroundColorAttributeName: validationPlaceHolderColor}];
        return false;
    }
    
    return true;
}

#pragma mark Action Sheet Delegate

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    [self openCamera];
                    break;
                case 1:
                    [self openGallery];
                    break;
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}
-(void)openCamera
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:NULL];
}
-(void)openGallery
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}

#pragma mark - Image Picker Delegates

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    imgView_EventImage.image = chosenImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
