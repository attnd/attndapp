//
//  TapToSeeOutViewController.h
//  Attnd
//
//  Created by Jatin Harish on 12/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TapToSeeOutViewController : UIViewController
{
    
    IBOutlet UIView *view_MessageToTap;
    
    IBOutlet UILabel *lbl_AlertMessage;
    
    IBOutlet UILabel *lbl_NoFriends;
    
    IBOutlet UICollectionView *colViewTapFriends;
}
@end
