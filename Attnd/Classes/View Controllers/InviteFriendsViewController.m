//
//  InviteFriendsViewController.m
//  Attnd
//
//  Created by Jatin Harish on 13/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import "InviteFriendsViewController.h"
#import "InviteCollectionViewCell.h"

@interface InviteFriendsViewController ()

@end

@implementation InviteFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //*-*-*-*-*-*- Shadow for bottom View
    CALayer *layer = view_Bottom.layer;
    layer.shadowOffset = CGSizeMake(1, 1);
    layer.shadowColor = [[UIColor blackColor] CGColor];
    layer.shadowRadius = 4.0f;
    layer.shadowOpacity = 0.80f;
    layer.shadowPath = [[UIBezierPath bezierPathWithRect:layer.bounds] CGPath];
    
    btnInviteAll.layer.cornerRadius= 5.0;
    btnInviteAll.layer.masksToBounds = YES;
    
    
    btnSend.layer.cornerRadius= 5.0;
    btnSend.layer.masksToBounds = YES;
}
#pragma mark - Collection View Delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
{
    return 23;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    InviteCollectionViewCell *cell = (InviteCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Invite" forIndexPath:indexPath];
    
    
    
    cell.lbl_friendName.text = @"Jatin Harish";
    
    
    cell.imgView_FriendDP.layer.cornerRadius=cell.imgView_FriendDP.frame.size.height/2;
    cell.imgView_FriendDP.layer.masksToBounds = YES;
    
    cell.imgView_Tickmark.layer.cornerRadius=cell.imgView_Tickmark.frame.size.height/2;
    cell.imgView_Tickmark.layer.masksToBounds = YES;
    
    //cell.imgView_Tickmark.image = [UIImage imageNamed:@"ic_tick"];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/4 -10 , 120);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    
    InviteCollectionViewCell *datasetCell = [colViewInviteFriends cellForItemAtIndexPath:indexPath];
    datasetCell.imgView_Tickmark.image=  [UIImage imageNamed:@"ic_tick"];
   
       
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
