//
//  FriendsListViewController.h
//  Attnd
//
//  Created by Gobinder Singh Sidhu on 4/14/17.
//  Copyright © 2017 Gobinder Sidhu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendsCollectionViewCell.h"

@interface FriendsListViewController : UIViewController
{

 
  NSMutableArray  *arrayfiltered;
     BOOL searchActive;
//    NSArray *searchArray;
}


@property (strong, nonatomic) IBOutlet UICollectionView *collView_friendList;

@property (strong, nonatomic) NSMutableArray *filteredArray;

@end
