//
//  ForgotPasswordViewController.m
//  Attnd
//
//  Created by Jatin Harish on 10/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "AccountVerificationViewController.h"
#import "CallAPIClass.h"



@interface ForgotPasswordViewController ()<WebAPIDelegate>
{
    CallAPIClass *objAPI;
}

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    objAPI = [[CallAPIClass alloc]init];
    objAPI.delegate = self;
    
    UIView *usernamePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    txtEmail.leftView = usernamePadding;
    txtEmail.leftViewMode = UITextFieldViewModeAlways;
    
  
    
    txtEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Your .edu email" attributes:@{NSForegroundColorAttributeName: TextfieldPlaceHolderColor}];
    
    txtEmail.layer.cornerRadius = 5.0;
    
      
}

#pragma mark - Button Action
- (IBAction)btnActionSend:(UIButton *)sender
{
    if ([self validate])
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        NSMutableDictionary  *forgotParam = [NSMutableDictionary dictionaryWithObjectsAndKeys:txtEmail.text, @"email",nil];
        
        [objAPI callServerAPI:forgotParam ServiceType:@"POST" ServiceURL:FORGOT];
  
  }
    
}
-(void)responseServerData:(NSDictionary *)responcedict
{
    NSLog(@"response: %@",responcedict);
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"success"])
    {
        
        AccountVerificationViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"AccountVerificationViewController"];
        controller.forgotEmail = [NSString stringWithFormat:@"%@",txtEmail.text];
        [self.navigationController pushViewController:controller animated:YES];
        
        
    }
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"error"])
    {
        
        [[Common sharedCommon] showAlertWithOkButton:@"" message:[NSString stringWithFormat:@"%@",[responcedict valueForKey:@"resMessage"]]];
    }
    else
    {
        
    }
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}
- (IBAction)btnActionBack:(UIButton *)sender
{
    NavigationBack
}
-(BOOL)validate
{
    if (txtEmail.text.length==0)
    {
        txtEmail.layer.borderWidth = 2.5;
        txtEmail.layer.borderColor = [UIColor redColor].CGColor;
        
        lbl_EmailError.hidden=NO;
        lbl_EmailError.text = @"Email should not be Blank";
        
        return false;
        
    }
    
    if (![self NSStringIsValidEmail:txtEmail.text])
    {
        txtEmail.layer.borderWidth = 2.5;
        txtEmail.layer.borderColor = [UIColor redColor].CGColor;
        
        lbl_EmailError.hidden=NO;
        lbl_EmailError.text = @"Email should be .edu format";
        return false;
    }

    lbl_EmailError.hidden=YES;
   
    txtEmail.layer.borderWidth = 0;
    txtEmail.layer.borderColor = [UIColor clearColor].CGColor;
  
    return true;
}

-(BOOL)NSStringIsValidEmail:(NSString *)checkString
{
    //Create a regex string
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.edu" ;
    
    //Create predicate with format matching your regex string
    NSPredicate *emailTest = [NSPredicate predicateWithFormat: @"SELF MATCHES %@", stricterFilterString];
    
    //return true if email address is valid
    return [emailTest evaluateWithObject:checkString];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
