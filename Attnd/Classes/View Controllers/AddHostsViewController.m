//
//  AddHostsViewController.m
//  Attnd
//
//  Created by Jatin Harish on 26/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import "AddHostsViewController.h"
#import "AddhostsCollectionViewCell.h"
#import "Constant.h"
#import "MBProgressHUD.h"
#import "CallAPIClass.h"
#import "friendListModal.h"
#import "Common.h"

@interface AddHostsViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,WebAPIDelegate>
{
    NSMutableArray *friendsName, *arrayfiltered,*arrayCheckBox, *arrayTosendBack;
    
    NSMutableArray *arrayFriends;
    
    BOOL searchActive;
    
    
    CallAPIClass *objAPI;
    
}

@end

@implementation AddHostsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    objAPI = [[CallAPIClass alloc]init];
    objAPI.delegate = self;
    
    
    //*-**-*-* Add Shadow to UIView (Bottom view)
    CALayer *layer = view_Bottom.layer;
    layer.shadowOffset = CGSizeMake(1, 1);
    layer.shadowColor = [[UIColor blackColor] CGColor];
    layer.shadowRadius = 4.0f;
    layer.shadowOpacity = 0.80f;
    layer.shadowPath = [[UIBezierPath bezierPathWithRect:layer.bounds] CGPath];
    
    
    arrayFriends = [[NSMutableArray alloc]init];
    
    arrayfiltered = [[NSMutableArray alloc]init];
    
    arrayCheckBox = [[NSMutableArray alloc]init];
    
    arrayTosendBack = [[NSMutableArray alloc]init];
    
    btnAddHosts.layer.cornerRadius = 5.0;
    
    btnAddHosts.layer.masksToBounds = YES;
    
    searchActive = NO;
    [self getfriendList];
}
-(void)viewWillAppear:(BOOL)animated
{
    
}

#pragma mark - Search bar delegates
- (void)searchBar:(UISearchBar *) searchBar textDidChange:(NSString *)searchText
{
    [arrayfiltered removeAllObjects];
    if (searchBar.text && [searchBar.text length])
    {
        searchActive = YES;
        
        NSMutableArray * arrFiltered = [[NSMutableArray alloc] init];
        for (int i = 0; i< arrayFriends.count; i++)
        {
            
            NSDictionary *tempdict  = [arrayFriends objectAtIndex:i];
            [arrFiltered addObject:tempdict];
        }
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",searchText];
        
        NSArray *filteredarray = [[arrFiltered valueForKey:@"friend_name"] filteredArrayUsingPredicate:predicate];
        
        if ([filteredarray count]==0)
        {
            [colView_AddHosts reloadData];
            
            [[Common sharedCommon] showAlertWithOkButton:@"" message:@"No Friends Found"];
            
        }
        for (int arrCount1=0; arrCount1<filteredarray.count; arrCount1++)
        {
            
            for (int arrCount=0; arrCount<arrayFriends.count; arrCount++)
            {
                NSString *tempstring = [NSString stringWithFormat:@"%@",[[arrayFriends objectAtIndex:arrCount] valueForKey:@"friend_name"]];
                
                NSLog(@"===%@",tempstring);
                
                NSString *comparestring = [filteredarray objectAtIndex:arrCount1];
                
                NSLog(@"===%@",comparestring);
                
                if([tempstring isEqualToString:comparestring])
                {
                    NSMutableDictionary *tempdict = [arrayFriends objectAtIndex:arrCount];
                    
                    [arrayfiltered addObject:tempdict];
                    //[colView_AddHosts reloadData];
                }
                else
                {
                    NSLog(@"xcfsc");
                }
                 [colView_AddHosts reloadData];
            }
            
        }
    }
    else
    {
        searchActive = NO;
        
    }
    [colView_AddHosts reloadData];
    
   
}
#pragma mark - Collection View Delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
{
    if (searchActive)
    {
        return arrayfiltered.count;
    }
    else
    {
        return arrayFriends.count;
    }
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    AddhostsCollectionViewCell *cell = (AddhostsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"addhosts" forIndexPath:indexPath];
    
    //*-*-*-*-*---**-* Set Corner Radius
    cell.imgView_FriendPic.layer.cornerRadius=cell.imgView_FriendPic.frame.size.height/2;
    cell.imgView_FriendPic.layer.masksToBounds = YES;
    cell.imgView_TickMark.layer.cornerRadius=cell.imgView_TickMark.frame.size.height/2;
    cell.imgView_TickMark.layer.masksToBounds = YES;
    
    
    
    
    [cell.btnTickMark addTarget:self action:@selector(ischecked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    //*-*-*-* set friend name according to search Active or Not
    if (searchActive)
    {
        friendListModal *objModal=[arrayfiltered objectAtIndex:indexPath.row];
        // cell.imgView_FriendPic.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kImgBaseURL,objModal.friend_ProfilePic]]]];
        NSMutableArray * matches = [[NSMutableArray alloc] init];
        for (int i = 0; i< arrayCheckBox.count; i++){
            [matches insertObject:[arrayCheckBox objectAtIndex:i] atIndex:i];
        }
        
        NSLog(@"%@",matches);
        if ([matches containsObject:objModal.friend_id])
        {
            cell.imgView_TickMark.image=  [UIImage imageNamed:@"ic_tick"];
        }
        else
        {
            cell.imgView_TickMark.image=  [UIImage imageNamed:@""];
            
        }
        
        cell.lbl_friendsName.text = objModal.friend_name;
        
    }
    else
    {
        friendListModal *objModal=[arrayFriends objectAtIndex:indexPath.row];
        //cell.imgView_FriendPic.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kImgBaseURL,objModal.friend_ProfilePic]]]];
        NSMutableArray * matches = [[NSMutableArray alloc] init];
        for (int i = 0; i< arrayCheckBox.count; i++){
            [matches insertObject:[arrayCheckBox objectAtIndex:i] atIndex:i];
        }
        
        NSLog(@"%@",matches);
        if ([matches containsObject:objModal.friend_id])
        {
            cell.imgView_TickMark.image=  [UIImage imageNamed:@"ic_tick"];
        }
        else
        {
            cell.imgView_TickMark.image=  [UIImage imageNamed:@""];
            
        }
        
        cell.lbl_friendsName.text = objModal.friend_name;
        
    }
    
    
    cell.btnTickMark.tag = indexPath.row;
    
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/4 -10 , 120);
}


//-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
//
//    AddhostsCollectionViewCell *datasetCell = [colView_AddHosts cellForItemAtIndexPath:indexPath];
//
//    datasetCell.imgView_TickMark.image=  [UIImage imageNamed:@"ic_tick"];
//
//    [arrayCheckBox addObject:[friendsName objectAtIndex:indexPath.row]];
//
//}

#pragma mark - Button Actions

- (IBAction)btnActionBack:(UIBarButtonItem *)sender {
    
    NavigationBack
}
- (IBAction)btnActionAddHosts:(UIButton *)sender
{
    
    [self.hostDelegate getHostsArray:arrayTosendBack];
    
    NSLog(@"Hosts array ====== %@",arrayCheckBox);
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (IBAction)ischecked:(UIButton *)sender
{
    
    AddhostsCollectionViewCell * cell = (AddhostsCollectionViewCell *)[[sender superview] superview];
    NSIndexPath *indexPath = [colView_AddHosts indexPathForCell:cell];
    NSLog(@"%ld",indexPath.row);
    
    UIButton *btn=(UIButton*)sender;
    btn.selected = !btn.selected;
    
    if (searchActive)
    {
        friendListModal *objModal=[arrayfiltered objectAtIndex:indexPath.row];
        if (btn.selected)
        {
            
            cell.imgView_TickMark.image=  [UIImage imageNamed:@"ic_tick"];
            [arrayCheckBox addObject:objModal.friend_id];
            [arrayTosendBack addObject:objModal];
            
        }
        else
        {
            cell.imgView_TickMark.image=  [UIImage imageNamed:@""];
            
            [arrayCheckBox removeObject:objModal.friend_id];
            [arrayTosendBack removeObject:objModal];
        }
        
    }
    else{
        
        friendListModal *objModal=[arrayFriends objectAtIndex:indexPath.row];
        if (btn.selected)
        {
            
            cell.imgView_TickMark.image=  [UIImage imageNamed:@"ic_tick"];
            [arrayCheckBox addObject:objModal.friend_id];
            [arrayTosendBack addObject:objModal];
            
        }
        else
        {
            cell.imgView_TickMark.image=  [UIImage imageNamed:@""];
            [arrayCheckBox removeObject:objModal.friend_id];
            [arrayTosendBack removeObject:objModal];
        }
    }
}

//- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath;
//{
//    AddhostsCollectionViewCell *datasetCell = [colView_AddHosts cellForItemAtIndexPath:indexPath];
//
//    datasetCell.imgView_TickMark.image=  [UIImage imageNamed:@""];
//
//    [arrayCheckBox removeObject:[friendsName objectAtIndex:indexPath.row]];
//
//}

#pragma mark - API Methods
-(void)getfriendList
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strConnectionURL = [NSString stringWithFormat:@"%@",GETFRIENDLIST];
    
    NSString *authorizationToken = [NSString stringWithFormat:@"BearerAttnd %@",SecurityToken];
    
    [objAPI callServerAPIwithHeader:nil ServiceType:@"GET" ServiceURL:strConnectionURL AutherisationToken:authorizationToken];
    
}
-(void)responseServerDataWithHeader:(NSDictionary *)responcedict
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"success"])
    {
        [arrayFriends removeAllObjects];
        
        //fetching array from Api
        NSMutableArray *fetched_Array= [[responcedict valueForKey:@"resData"] valueForKey:@"friends"];
        
        if (fetched_Array.count == 0)
        {
            lbl_NoFriends.hidden = NO;
            lbl_NoFriends.text = @"No Friends";
        }
        
        if (fetched_Array.count>0) {
            
            for (int i=0; i<fetched_Array.count; i++)
            {
                NSMutableDictionary *fetched_Dic=[fetched_Array objectAtIndex:i];
                friendListModal *objModal=[[friendListModal alloc]init];
                objModal= [objModal initWithDictionary:fetched_Dic];
                
                // Adding The model Obj to Final Array
                [arrayFriends addObject:objModal];
            }
            [colView_AddHosts reloadData];
            
            
        }
        if ([[responcedict valueForKey:@"resStatus"] isEqual:@"error"])
        {
            [[Common sharedCommon] showAlertWithOkButton:@"" message:[NSString stringWithFormat:@"%@",[responcedict valueForKey:@"resMessage"]]];
        }
        
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
