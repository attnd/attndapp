//
//  LoginViewController.h
//  Attnd
//
//  Created by Jatin Harish on 10/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface LoginViewController : UIViewController
{
    
    IBOutlet UITextField *txtEmail;
    
    IBOutlet UITextField *txtPassword;
    
    //Linking for label Error messages
    IBOutlet UILabel *lbl_ErrorUsername;
    
    IBOutlet UILabel *lbl_ErrorPassword;
}

@end
