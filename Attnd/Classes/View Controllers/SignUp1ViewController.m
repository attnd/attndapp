//
//  SignUp1ViewController.m
//  Attnd
//
//  Created by Jatin Harish on 11/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import "SignUp1ViewController.h"
#import "Constant.h"
#import "LastStepViewController.h"
#import "AccountVerificationViewController.h"
#import "MBProgressHUD.h"
#import "LastStepViewController.h"

@interface SignUp1ViewController ()

@end

@implementation SignUp1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self loadInitialView];
}
#pragma  mark - Button Action

- (IBAction)btnActionLogin:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:NO];
}
- (IBAction)btnActionNext:(UIButton *)sender {

    
    if ([self validate])
    {
        NSMutableDictionary *tempdict  = [[NSMutableDictionary alloc]init];
        [tempdict setObject:[NSString stringWithFormat:@"%@",txt_RealName.text] forKey:@"realName"];
        [tempdict setObject:[NSString stringWithFormat:@"%@",txt_Username.text] forKey:@"userName"];
        [tempdict setObject:[NSString stringWithFormat:@"%@",txt_Email.text] forKey:@"email"];
        [tempdict setObject:[NSString stringWithFormat:@"%@",txt_Password.text] forKey:@"password"];
        
        LastStepViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"LastStepViewController"];
        
        controller.arrayRegisterData = tempdict;
        
        [self.navigationController pushViewController:controller animated:YES];
        
 
    }
    
}
-(BOOL)validate
{
    if (txt_Email.text.length==0 && txt_Password.text.length==0 && txt_Username.text.length==0 && txt_RealName.text.length==0)
    {
        txt_RealName.layer.borderWidth = 2.5;
        txt_RealName.layer.borderColor = [UIColor redColor].CGColor;
        lbl_realNameError.hidden=NO;
        lbl_realNameError.text = @"Real Name should not be Blank";
        
        txt_Username.layer.borderWidth = 2.5;
        txt_Username.layer.borderColor = [UIColor redColor].CGColor;
        lbl_userNameError.hidden=NO;
        lbl_userNameError.text = @"User Name should not be Blank";
        
        txt_Email.layer.borderWidth = 2.5;
        txt_Email.layer.borderColor = [UIColor redColor].CGColor;
        lbl_emailError.hidden=NO;
        lbl_emailError.text = @"Email should not be Blank";
        
        txt_Password.layer.borderWidth = 2.5;
        txt_Password.layer.borderColor = [UIColor redColor].CGColor;
        lbl_PasswordError.hidden=NO;
        lbl_PasswordError.text = @"Email should not be Blank";

        return false;
        
    }
    
    if (txt_Email.text.length==0 || txt_Password.text.length==0 || txt_Username.text.length==0 || txt_RealName.text.length==0)
    {
        if (txt_RealName.text.length==0)
        {
            txt_RealName.layer.borderWidth = 2.5;
            txt_RealName.layer.borderColor = [UIColor redColor].CGColor;
            lbl_realNameError.hidden=NO;
            lbl_realNameError.text = @"Real Name should not be Blank";
            
            lbl_userNameError.hidden=YES;
            txt_Username.layer.borderWidth = 0;
            txt_Username.layer.borderColor = [UIColor clearColor].CGColor;

            lbl_emailError.hidden=YES;
            txt_Email.layer.borderWidth = 0;
            txt_Email.layer.borderColor = [UIColor clearColor].CGColor;

            lbl_PasswordError.hidden=YES;
            txt_Password.layer.borderWidth = 0;
            txt_Password.layer.borderColor = [UIColor clearColor].CGColor;
        }
        else  if (txt_Username.text.length==0)
        {
            txt_Username.layer.borderWidth = 2.5;
            txt_Username.layer.borderColor = [UIColor redColor].CGColor;
            lbl_userNameError.hidden=NO;
            lbl_userNameError.text = @"User Name should not be Blank";
            
            lbl_realNameError.hidden=YES;
            txt_RealName.layer.borderWidth = 0;
            txt_RealName.layer.borderColor = [UIColor clearColor].CGColor;
            
            lbl_emailError.hidden=YES;
            txt_Email.layer.borderWidth = 0;
            txt_Email.layer.borderColor = [UIColor clearColor].CGColor;
            
            lbl_PasswordError.hidden=YES;
            txt_Password.layer.borderWidth = 0;
            txt_Password.layer.borderColor = [UIColor clearColor].CGColor;
        }
        else  if (txt_Email.text.length==0)
        {
            txt_Email.layer.borderWidth = 2.5;
            txt_Email.layer.borderColor = [UIColor redColor].CGColor;
            lbl_emailError.hidden=NO;
            lbl_emailError.text = @"Email should not be Blank";
            
            lbl_realNameError.hidden=YES;
            txt_RealName.layer.borderWidth = 0;
            txt_RealName.layer.borderColor = [UIColor clearColor].CGColor;
            
            lbl_userNameError.hidden=YES;
            txt_Username.layer.borderWidth = 0;
            txt_Username.layer.borderColor = [UIColor clearColor].CGColor;
            
            lbl_PasswordError.hidden=YES;
            txt_Password.layer.borderWidth = 0;
            txt_Password.layer.borderColor = [UIColor clearColor].CGColor;
        }
        else  if (txt_Password.text.length==0)
        {
            txt_Password.layer.borderWidth = 2.5;
            txt_Password.layer.borderColor = [UIColor redColor].CGColor;
            lbl_PasswordError.hidden=NO;
            lbl_PasswordError.text = @"Password should not be Blank";
            
            lbl_realNameError.hidden=YES;
            txt_RealName.layer.borderWidth = 0;
            txt_RealName.layer.borderColor = [UIColor clearColor].CGColor;
            
            lbl_userNameError.hidden=YES;
            txt_Username.layer.borderWidth = 0;
            txt_Username.layer.borderColor = [UIColor clearColor].CGColor;
            
            lbl_emailError.hidden=YES;
            txt_Email.layer.borderWidth = 0;
            txt_Email.layer.borderColor = [UIColor clearColor].CGColor;
        }

        return false;
    }
//    if (![self NSStringIsValidEmail:txt_Email.text])
//    {
//        txt_Email.layer.borderWidth = 2.5;
//        txt_Email.layer.borderColor = [UIColor redColor].CGColor;
//        lbl_emailError.hidden=NO;
//        lbl_emailError.text = @"Email should be .edu format";
//        
//        lbl_realNameError.hidden=YES;
//        txt_RealName.layer.borderWidth = 0;
//        txt_RealName.layer.borderColor = [UIColor clearColor].CGColor;
//        
//        lbl_userNameError.hidden=YES;
//        txt_Username.layer.borderWidth = 0;
//        txt_Username.layer.borderColor = [UIColor clearColor].CGColor;
//        
//        lbl_PasswordError.hidden=YES;
//        txt_Password.layer.borderWidth = 0;
//        txt_Password.layer.borderColor = [UIColor clearColor].CGColor;
//        
//        return false;
//    }
    txt_RealName.layer.borderWidth = 0;
    txt_RealName.layer.borderColor = [UIColor clearColor].CGColor;
    txt_Username.layer.borderWidth = 0;
    txt_Username.layer.borderColor = [UIColor clearColor].CGColor;
    txt_Email.layer.borderWidth = 0;
    txt_Email.layer.borderColor = [UIColor clearColor].CGColor;
    txt_Password.layer.borderWidth = 0;
    txt_Password.layer.borderColor = [UIColor clearColor].CGColor;
    
    
    lbl_realNameError.hidden = YES;
    lbl_userNameError.hidden = YES;
    lbl_emailError.hidden = YES;
    lbl_PasswordError.hidden = YES;
    
    return true;

}
#pragma mark - Validation Methods
-(BOOL)NSStringIsValidEmail:(NSString *)checkString
{
    //Create a regex string
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.edu" ;
    
    //Create predicate with format matching your regex string
    NSPredicate *emailTest = [NSPredicate predicateWithFormat: @"SELF MATCHES %@", stricterFilterString];
    
    //return true if email address is valid
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark - custom Methods
-(void)loadInitialView
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    txt_RealName.autocapitalizationType = UITextAutocapitalizationTypeWords;
    txt_Username.autocapitalizationType = UITextAutocapitalizationTypeWords;
    //txt_Email.autocapitalizationType = UITextAutocapitalizationTypeWords;
    //txt_Password.autocapitalizationType = UITextAutocapitalizationTypeWords;

    
    UIView *realNamePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    txt_RealName.leftView = realNamePadding;
    txt_RealName.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *usernamePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    txt_Username.leftView = usernamePadding;
    txt_Username.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *emailPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    txt_Email.leftView = emailPadding;
    txt_Email.leftViewMode = UITextFieldViewModeAlways;

    UIView *passwordPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    txt_Password.leftView = passwordPadding;
    txt_Password.leftViewMode = UITextFieldViewModeAlways;

    
    txt_RealName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Real name" attributes:@{NSForegroundColorAttributeName: TextfieldPlaceHolderColor}];
    
    txt_RealName.layer.cornerRadius = 5.0;
    
    
    txt_Username.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: TextfieldPlaceHolderColor}];
    
    txt_Username.layer.cornerRadius = 5.0;

    
    
    txt_Email.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@".edu email" attributes:@{NSForegroundColorAttributeName: TextfieldPlaceHolderColor}];
    
    txt_Email.layer.cornerRadius = 5.0;
    
    txt_Password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: TextfieldPlaceHolderColor}];
    
    txt_Password.layer.cornerRadius = 5.0;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
