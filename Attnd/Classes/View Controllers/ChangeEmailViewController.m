//
//  ChangeEmailViewController.m
//  Attnd
//
//  Created by Gobinder Singh Sidhu on 4/14/17.
//  Copyright © 2017 Gobinder Sidhu. All rights reserved.
//

#import "ChangeEmailViewController.h"

@interface ChangeEmailViewController ()

@end

@implementation ChangeEmailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    // Do any additional setup after loading the view.
}

-(void)alertViewController :(NSString *)message withTitle: (NSString *) title
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
  
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   //do something when click button
                               }];
   
    [alert addAction:okAction];
     [self presentViewController:alert animated:YES completion:nil];
    
    
    
}


-(BOOL)validate
{
    
    
    if (![self NSStringIsValidEmail:_txt_currentEmail.text])
    {
        [self alertViewController:@"Invalid current email" withTitle:@""];
        return false;
    }
    
   else if (![self NSStringIsValidEmail:_txt_newEmail.text] )
    {
        [self alertViewController:@"Invalid new email" withTitle:@""];
        return false;
    }
    
    else  if (self.txt_Password.text.length==0 )
    {
        [self alertViewController:@"Password should not be empty" withTitle:@""];
        
        return false;
    }
    
    
    return true;
 
}


-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (IBAction)btn_SaveAction:(id)sender {
    
    if ([self validate])
    {
        
    }
    
    
    

    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
