//
//  FriendsVC.h
//  Attnd
//
//  Created by Gobinder Singh Sidhu on 4/11/17.
//  Copyright © 2017 Gobinder Sidhu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuartzCore/CALayer.h"

#import "FriendsProfileCollectionViewCell.h"
#import "FriendsListViewController.h"
#import "ProfileVC.h"
#import "TabBarViewController.h"



@protocol FriendsVCDelegate <NSObject>

//-(void)sendBtnAction;

@end

@interface FriendsVC : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>
{

    IBOutlet UILabel *lbl_Nofriends;

}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView_friends;
@property (retain ,nonatomic) NSMutableArray *arrayFriends;
@end
