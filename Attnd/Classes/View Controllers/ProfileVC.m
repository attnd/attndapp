//
//  ProfileVC.m
//  Attnd
//
//  Created by Gobinder Singh Sidhu on 4/10/17.
//  Copyright © 2017 Gobinder Sidhu. All rights reserved.
//

#import "ProfileVC.h"
#import "FriendsVC.h"
#import "PhotosVC.h"
#import "EventsViewController.h"
#import "CAPSPageMenu.h"
#import <Photos/Photos.h>
#import "SettingViewController.h"
#import "Constant.h"
#import "CallAPIClass.h"
#import "MBProgressHUD.h"
#import "Common.h"



@interface ProfileVC ()<WebAPIDelegate>
{
    CallAPIClass *objAPI;
    //*-*-*-* controls for CAPSAPMenu
    NSArray *controllerArray;
    NSDictionary *CAPSPparameters;
    //*-*-*-*-*-*
    
    PhotosVC *PhotosView;
    FriendsVC *FriendsView;
    EventsViewController *EventsView;
}
@property (nonatomic) CAPSPageMenu *pageMenu;
@end

@implementation ProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
   
    objAPI = [[CallAPIClass alloc]init];
    objAPI.delegate = self;
    
    self.navigationController.navigationBar.topItem.title = @"Profile";
    btnTypeArray = [[NSMutableArray alloc]init];
    [btnTypeArray addObject:@"2"];
     [btnTypeArray addObject:@"6"];
     [btnTypeArray addObject:@"4"];
    
    PhotosView = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotosVC"];
    PhotosView.title = @"PHOTOS";
    
    FriendsView = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendsVC"];
    FriendsView.title = @"FRIENDS";
    
    EventsView = [self.storyboard instantiateViewControllerWithIdentifier:@"EventsViewController"];
    EventsView.title = @"EVENTS";
    
    
   controllerArray = @[PhotosView,FriendsView,EventsView];
   CAPSPparameters = @{
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionSelectionIndicatorColor: [UIColor blueColor],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:70.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"HelveticaNeue" size:13.0],
                                 CAPSPageMenuOptionMenuHeight: @(40.0),
                                 CAPSPageMenuOptionMenuItemWidth: @(self.view.frame.size.width/3),
                                 CAPSPageMenuOptionCenterMenuItems: @(YES)
                                 };

        _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(4, 355, self.view.frame.size.width-8, self.view.frame.size.height-397) options:CAPSPparameters];
        [self.view addSubview:_pageMenu.view];
        _btn_addPic.hidden=NO;
        _btn_deletePic.hidden=NO;
        _imgvw_addPic.hidden=NO;
        _imgvw_deletePic.hidden=NO;
    
    [[_imageView_profilePic layer] setCornerRadius:_imageView_profilePic.frame.size.width/2 ];
    [[_imageView_profilePic layer] setMasksToBounds:YES];
    
   // [self getProfileData];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self getProfileData];
}


-(BOOL)hidesBottomBarWhenPushed
{
    return NO;
}

#pragma mark- ------------------Collection View Data source------------------

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return btnTypeArray.count;
}

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    
//   // return CGSizeMake(self.view.frame.size.width/3 -10 , 120);
//    
//    CGFloat viewSize = (self.view.bounds.size.width)/3-50;
//    return CGSizeMake(viewSize, viewSize *1.2);
//}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/3 -10 , 120);
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"ProfileCentreCollectionViewCell";
    ProfileCentreCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
   // cell.imageView_btnBckg.image = [UIImage imageNamed:@"samplePic"];
     [cell.imageView_btnBckg setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"samplePic"]]];
   // [cell.imageView_btnBckg setBackgroundColor:[UIColor greenColor]];
    
    [[cell.imageView_btnBckg layer] setCornerRadius:cell.imageView_btnBckg.frame.size.width/2];
    [[cell.imageView_btnBckg layer] setMasksToBounds:YES];
    

    // shadow
    cell.imageView_btnBckg.layer.shadowColor = [UIColor grayColor].CGColor;
    cell.imageView_btnBckg.layer.shadowOffset = CGSizeMake(0, 2);
    cell.imageView_btnBckg.layer.shadowOpacity = 1;
    cell.imageView_btnBckg.layer.shadowRadius = 1.0;
    cell.imageView_btnBckg.clipsToBounds = NO;
    
    cell.label_name.text = [ btnTypeArray objectAtIndex:indexPath.row ];
    
    
    return cell;
    
}

#pragma mark - Button Actions


- (IBAction)btnAction_Settings:(UIBarButtonItem *)sender
{
    
    UINavigationController *nav = (UINavigationController *)self.parentViewController.view.window.rootViewController;
    
    SettingViewController *contorller = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"];
    
    [nav pushViewController:contorller animated:YES];
 
}

- (IBAction)btnAction_uploadImage:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
         [self takePhoto];
        // Cancel button tappped do nothing.
        
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self chooseExistingPic];
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // take photo button tapped.
      
        
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}



-(void)takePhoto
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                          message:@"Device has no camera"
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles: nil];
    
    [myAlertView show];
    
   }
   else
{

    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:imagePicker animated:YES completion:NULL];
}

}


-(void)chooseExistingPic
{
    
    imagePicker = [[UIImagePickerController alloc]init];
    // Check if image access is authorized
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        // Use delegate methods to get result of photo library -- Look up UIImagePicker delegate methods
        imagePicker.delegate = self;
        [self presentViewController:imagePicker animated:true completion:nil];
    }
    
}


- (IBAction)btn_Action_deleteImage:(id)sender {
    
   
    
    _imageView_profilePic.image = nil;
    
}

- (IBAction)btn_action_shareInstagram:(id)sender {
    
    
    
}

- (IBAction)btn_action_shareTwitter:(id)sender {
    
    
    
}

- (IBAction)btn_action_shareSnapchat:(id)sender {
    
    
    
}
- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}
#pragma image Picker Delegates

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    _imageView_profilePic.image = image;
    
    [self updateProfilePicMethod];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
-(void)updateProfilePicMethod
{
    
    NSData *imgData=UIImageJPEGRepresentation([self imageWithImage:_imageView_profilePic.image convertToSize:CGSizeMake(400, 400)], 0.9);
    NSString *strConnectionURL = [NSString stringWithFormat:@"%@",UPLOAD_PROFILEPIC];
    
    NSString *authorisationToken =  [NSString stringWithFormat:@"BearerAttnd %@",SecurityToken];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [objAPI callServerAPIwithMultipart:nil ServiceType:@"POST" ServiceURL:strConnectionURL AutherisationToken:authorisationToken ImgData:imgData ImgParameter:@"profilePic"];
  
}
#pragma mark - API Method
-(void)getProfileData
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strConnectionURL = [NSString stringWithFormat:@"%@/%@",GETPROFILE,UserID];
    
    NSString *authorizationToken = [NSString stringWithFormat:@"BearerAttnd %@",SecurityToken];
    
    [objAPI callServerAPIwithHeader:nil ServiceType:@"GET" ServiceURL:strConnectionURL AutherisationToken:authorizationToken];
    
}
#pragma mark - API METHOD
-(void)responseServerData:(NSDictionary *)responcedict
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"success"])
    {
    [[Common sharedCommon] showAlertWithOkButton:@"" message:[NSString stringWithFormat:@"%@",[responcedict valueForKey:@"resMessage"]]];
    }
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"error"])
    {
        
    }
}

-(void)responseServerDataWithHeader:(NSDictionary *)responcedict
{
    NSLog(@"response: %@",responcedict);
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"success"])
    {
        //*-*-* Set data on label
        
        lbl_realName.text = [[[responcedict valueForKey:@"resData"] valueForKey:@"user"] valueForKey:@"realName"];
        
        lbl_userName.text = [NSString stringWithFormat:@"@%@",[[[responcedict valueForKey:@"resData"] valueForKey:@"user"] valueForKey:@"username"]];
        
        lbl_location.text = [NSString stringWithFormat:@"%@",[[[responcedict valueForKey:@"resData"] valueForKey:@"user"] valueForKey:@"location"]];
        
        lbl_age.text = [NSString stringWithFormat:@"%@ years old",[[[responcedict valueForKey:@"resData"] valueForKey:@"user"] valueForKey:@"age"]];
        
        lbl_relationshipStatus.text =[NSString stringWithFormat:@"%@",[[[responcedict valueForKey:@"resData"] valueForKey:@"user"] valueForKey:@"relationshipStatus"]];
        
        
        if([[[[responcedict valueForKey:@"resData"] valueForKey:@"user"] valueForKey:@"profilePic"]  isEqualToString:@""])
        {
            _imageView_profilePic.image = [UIImage imageNamed:@"defualt.png"];
        }
        else
        {
           _imageView_profilePic.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kImgBaseURL,[[[responcedict valueForKey:@"resData"] valueForKey:@"user"] valueForKey:@"profilePic"]]]]];
        }
                    
        PhotosView.arrayUserPhotos = [[[responcedict valueForKey:@"resData"] valueForKey:@"user"] valueForKey:@"images"];
        
        FriendsView.arrayFriends = [[responcedict valueForKey:@"resData"] valueForKey:@"friends"];
        
        
        EventsView.arrayEvents = [[responcedict valueForKey:@"resData"] valueForKey:@"events"];
        
        if([[[responcedict valueForKey:@"resData"] valueForKey:@"friendshipStatus"] isEqualToString:@"self profile"])
        {
            _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(4, 266, self.view.frame.size.width-8, self.view.frame.size.height-305) options:CAPSPparameters];
            [self.view addSubview:_pageMenu.view];
            
            _btn_addPic.hidden=NO;
            _btn_deletePic.hidden=NO;
            _imgvw_addPic.hidden=NO;
            _imgvw_deletePic.hidden=NO;
            
            
        }
        else
        {
            _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(4, 355, self.view.frame.size.width-8, self.view.frame.size.height-397) options:CAPSPparameters];
            [self.view addSubview:_pageMenu.view];
            
            _btn_addPic.hidden=YES;
            _btn_deletePic.hidden=YES;
            _imgvw_addPic.hidden=YES;
            _imgvw_deletePic.hidden=YES;
        }

    }
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"error"])
    {
        
        
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
