//
//  PhotosVC.h
//  Attnd
//
//  Created by Gobinder Singh Sidhu on 4/11/17.
//  Copyright © 2017 Gobinder Sidhu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuartzCore/CALayer.h"
#import "PhotosCollectionViewCell.h"



@interface PhotosVC : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
{
    
    IBOutlet UILabel *lbl_NoPhotos;

}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView_photos;
@property (retain, nonatomic) NSMutableArray *arrayUserPhotos;
@end
