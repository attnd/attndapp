//
//  EventsViewController.h
//  Attnd
//
//  Created by Jatin Harish on 12/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "CAPSPageMenu.h"


@interface EventsViewController : UIViewController


{
   
    IBOutlet UILabel *lbl_NoEvents;
}

//@property (strong,nonatomic) HomeViewController *homeTab;
@property (retain, nonatomic) NSMutableArray *arrayEvents;
@end
