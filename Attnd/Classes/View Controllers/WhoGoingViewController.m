//
//  WhoGoingViewController.m
//  Attnd
//
//  Created by Jatin Harish on 14/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import "WhoGoingViewController.h"
#import "SearchFriendCollectionViewCell.h"

@interface WhoGoingViewController ()

@end

@implementation WhoGoingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
#pragma mark - Collection View Delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
{
    return 23;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    SearchFriendCollectionViewCell *cell = (SearchFriendCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"SearchFriend" forIndexPath:indexPath];
    
    
    //    cell.imgView_SelectCoins.image
    
    cell.lbl_friendName.text = @"Jatin Harish";
    
    
    cell.imgView_FriendDP.layer.cornerRadius=cell.imgView_FriendDP.frame.size.height/2;
    cell.imgView_FriendDP.layer.masksToBounds = YES;
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/4 -10 , 120);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
