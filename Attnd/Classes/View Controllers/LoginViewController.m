//
//  LoginViewController.m
//  Attnd
//
//  Created by Jatin Harish on 10/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import "LoginViewController.h"
#import "Constant.h"
#import "AppDelegate.h"
#import "ForgotPasswordViewController.h"
#import "SignUp1ViewController.h"
#import "TabBarViewController.h"
#import "SearchFriendViewController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "FBUserDetails.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "Constant.h"
#import "CallAPIClass.h"
#import "Common.h"



@interface LoginViewController ()<WebAPIDelegate>
{
    CallAPIClass *objAPI;
    Common *objCommon;
    BOOL isSecurePassword;
}

@end

@implementation LoginViewController


-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    objAPI = [[CallAPIClass alloc]init];
    objAPI.delegate = self;
    
    objCommon = [Common getInstance];
    isSecurePassword = YES;
  
    txtEmail.text = @"avinash.thakur@mobilyte.edu";
    txtPassword.text = @"123456";
    txtPassword.secureTextEntry = YES;
//    txtEmail.text = @"jtn@dsf.edu";
//    txtPassword.text = @"123456";
   
    
    [self loadInitialView];
    
    
}
#pragma mark - Button Action

- (IBAction)btnActionSignUp:(UIButton *)sender
{
    SignUp1ViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUp1ViewController"];
    [self.navigationController pushViewController:controller animated:NO];
}

- (IBAction)btnActionLogin:(UIButton *)sender
{
    if ([self validate])
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
     
        NSMutableDictionary *loginParam = [NSMutableDictionary dictionaryWithObjectsAndKeys:txtEmail.text, @"email", txtPassword.text, @"password", [NSString stringWithFormat:@"%@",objCommon.device_Token] ,@"deviceToken", nil];
        
        [objAPI callServerAPI:loginParam ServiceType:@"POST" ServiceURL:LOGIN];
        
    }
    
}

- (IBAction)btnActionForgotPassword:(UIButton *)sender
{
    ForgotPasswordViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)btnActionFacebookLogin:(UIButton *)sender
{

    
}
#pragma mark - API METHOD
-(void)facebookOpen
{
    NSArray *permissions = [[NSArray alloc] initWithObjects:
                            @"email", @"public_profile", @"user_friends",
                            nil];
    FBSDKLoginManager* loginManager;
    if (!loginManager) {
        loginManager = [[FBSDKLoginManager alloc] init];
    }
    
    [loginManager logInWithReadPermissions:permissions
                        fromViewController:self
                                   handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                       
                                       NSLog(@"Login End");
                                       
                                       FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"first_name,id,friends,age_range,relationship_status,picture"}];
                                       [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
                                        {
                                            
                                            if (error)
                                            {
                                                NSLog(@"Error ==%@ ",error);
                                                
                                                
                                            }
                                            
                                            if (!error && result)
                                                
                                            {
                                                
                                                NSArray *temparray = [[result valueForKey:@"friends"] valueForKey:@"data"];
                                                
                                                ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
                                                
                                                objCommon.facebookFriends = [temparray mutableCopy];
                                                
                                                //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
                                                        TabBarViewController *contorller = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
                                                        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                                        appDelegate.tabBarController = contorller;
                                                        [self.navigationController pushViewController:contorller animated:YES];
                                                
                                                
                                                
                                            }
                                            else
                                            {
                                                
                                                
                                                
                                                
                                            }
                                        }];
                                   }];
    
    
    
}
- (IBAction)btnActionQuestionMark:(UIButton *)sender
{
    if (isSecurePassword == YES)
    {
        txtPassword.secureTextEntry = NO;
        isSecurePassword = NO;
        
    }
    else if (isSecurePassword == NO)
    {
        txtPassword.secureTextEntry = YES;
        isSecurePassword = YES;
        
    }
    
}

-(void)responseServerData:(NSDictionary *)responcedict
{
    NSLog(@"response: %@",responcedict);
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"success"])
    {
        NSDictionary *apiResult = [responcedict valueForKey:@"resData"];
        
        //*-*-*- Save the user token
        [DEFAULTS setValue:[apiResult valueForKey:@"token"] forKey:@"Securitytoken"];
        //*-*-*- Save the user ID
        [DEFAULTS setValue:[[apiResult valueForKey:@"user"] valueForKey:@"_id"] forKey:@"userid"];
        //*-*-*- Save the profilePic
        [DEFAULTS setValue:[[apiResult valueForKey:@"user"] valueForKey:@"profilePic"] forKey:@"ProfilePic"];
        
        
        TabBarViewController *contorller = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.tabBarController = contorller;
        [self.navigationController pushViewController:contorller animated:YES];
    }
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"error"])
    {
        
        txtEmail.layer.borderWidth = 2.5;
        txtEmail.layer.borderColor = [UIColor redColor].CGColor;
        
        lbl_ErrorUsername.hidden=NO;
        lbl_ErrorUsername.text = [NSString stringWithFormat:@"%@",[responcedict valueForKey:@"resMessage"]];
    }
    else
    {
    }
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}


#pragma mark - Validation Method
-(BOOL)NSStringIsValidEmail:(NSString *)checkString
{
    //Create a regex string
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.edu" ;
    
    //Create predicate with format matching your regex string
    NSPredicate *emailTest = [NSPredicate predicateWithFormat: @"SELF MATCHES %@", stricterFilterString];
    
    //return true if email address is valid
    return [emailTest evaluateWithObject:checkString];
}

-(BOOL)validate
{
    if (txtEmail.text.length==0 && txtPassword.text.length==0)
    {
        txtEmail.layer.borderWidth = 2.5;
        txtEmail.layer.borderColor = [UIColor redColor].CGColor;
        
        lbl_ErrorUsername.hidden=NO;
        lbl_ErrorUsername.text = @"Username should not be Blank";
        txtPassword.layer.borderWidth = 2.5;
        txtPassword.layer.borderColor = [UIColor redColor].CGColor;
        
        lbl_ErrorPassword.hidden=NO;
        lbl_ErrorPassword.text = @"Password should not be Blank";
        return false;

    }
    if (txtEmail.text.length==0 || txtPassword.text.length==0)
    {
        if (txtEmail.text.length==0)
        {
            txtEmail.layer.borderWidth = 2.5;
            txtEmail.layer.borderColor = [UIColor redColor].CGColor;
            
            lbl_ErrorUsername.hidden=NO;
            lbl_ErrorUsername.text = @"Username should not be Blank";

            lbl_ErrorPassword.hidden=YES;
            txtPassword.layer.borderWidth = 0;
            txtPassword.layer.borderColor = [UIColor clearColor].CGColor;
        }
        else if (txtPassword.text.length==0)
        {
            txtPassword.layer.borderWidth = 2.5;
            txtPassword.layer.borderColor = [UIColor redColor].CGColor;
            lbl_ErrorPassword.hidden=NO;
            lbl_ErrorPassword.text = @"Password should not be Blank";
            lbl_ErrorUsername.hidden=YES;
            txtEmail.layer.borderWidth = 0;
            txtEmail.layer.borderColor = [UIColor clearColor].CGColor;
            
            
        }
        
        return false;
        
        
    }
//    if (![self NSStringIsValidEmail:txtEmail.text])
//    {
//        txtEmail.layer.borderWidth = 2.5;
//        txtEmail.layer.borderColor = [UIColor redColor].CGColor;
//        
//        lbl_ErrorUsername.hidden=NO;
//        lbl_ErrorUsername.text = @"Email should be .edu format";
//        
//        lbl_ErrorPassword.hidden=YES;
//        txtPassword.layer.borderWidth = 0;
//        txtPassword.layer.borderColor = [UIColor clearColor].CGColor;
//        
//        return false;
//    }
    
    lbl_ErrorUsername.hidden=YES;
    lbl_ErrorPassword.hidden=YES;
    txtEmail.layer.borderWidth = 0;
    txtEmail.layer.borderColor = [UIColor clearColor].CGColor;
    txtPassword.layer.borderWidth = 0;
    txtPassword.layer.borderColor = [UIColor clearColor].CGColor;
    return true;
}
#pragma mark - Custom Methods
-(void)loadInitialView
{
    UIView *usernamePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    txtEmail.leftView = usernamePadding;
    txtEmail.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *passwordPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    txtPassword.leftView = passwordPadding;
    txtPassword.leftViewMode = UITextFieldViewModeAlways;
    
    txtEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username or .edu email" attributes:@{NSForegroundColorAttributeName: TextfieldPlaceHolderColor}];
    
    txtEmail.layer.cornerRadius = 5.0;
    
    
    txtPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: TextfieldPlaceHolderColor}];
    txtPassword.layer.cornerRadius = 5.0;
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//**-*-*-*

//    NSArray *permissions = [[NSArray alloc] initWithObjects:
//                            @"email", @"public_profile", @"user_friends",
//                            nil];
//    FBSDKLoginManager* loginManager;
//    if (!loginManager) {
//        loginManager = [[FBSDKLoginManager alloc] init];
//    }
//
//    // The FBSDKLoginManager sets this token for you
//    // and when it sets currentAccessToken it also automatically writes it to a keychain cache.
//    [loginManager logInWithReadPermissions:permissions
//                        fromViewController:self
//                                   handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
//
//                                       NSLog(@"Login End");
//
//                                       FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"first_name,id,friends"}];
//                                       [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
//                                           if (!error && result) {
//                            FBUserDetails *userDetails = [[FBUserDetails alloc] init];
//                                               userDetails.fbid = [[result objectForKey:@"id"] longLongValue];
//                                               userDetails.first_name = [result objectForKey:@"first_name"];
//
//                                               // After fetch me data, it will be cached
////                                               if (!meDetails) {
////                                                   meDetails = [[FBUserDetails alloc] init];
////                                               }
////                                               meDetails.fbid = userDetails.fbid;
////                                               meDetails.first_name = userDetails.first_name;
////
////                                               callback(userDetails);
//                                           }
//                                           else {
////                                               callback(nil);
//                                           }
//                                       }];
//                                   }];

@end
