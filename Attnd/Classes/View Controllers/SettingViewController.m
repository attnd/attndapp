//
//  SettingViewController.m
//  Attnd
//
//  Created by Gobinder Singh Sidhu on 4/14/17.
//  Copyright © 2017 Gobinder Sidhu. All rights reserved.
//

#import "SettingViewController.h"
#import "Constant.h"
#import "ChangeProfileDataViewController.h"


@interface SettingViewController ()

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    settingArray = [[NSMutableArray alloc]init];
    [settingArray addObject:@"Real name"];
    [settingArray addObject:@"Username"];
    [settingArray addObject:@"Change email"];
    [settingArray addObject:@"Change password"];
    [settingArray addObject:@"Change location"];
    [settingArray addObject:@"Change age"];
    [settingArray addObject:@"Change relationship status"];
    [settingArray addObject:@"System notifiations"];
    [settingArray addObject:@"Instagram"];
    [settingArray addObject:@"Twitter"];
    [settingArray addObject:@"Snapchat"];
    [settingArray addObject:@"Rate Attnd"];
    [settingArray addObject:@"Contact Us"];
    [settingArray addObject:@"Logout"];
    
    
    
    // Do any additional setup after loading the view.
}


#pragma mark- ------------------Table View Data source------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:
(NSInteger)section{
    
    
    return 14;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row ==0 || indexPath.row == 1) {
        
        return 70;
    }
    else
    {
    return 45;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:
(NSIndexPath *)indexPath{
    
 
    static NSString *BorderIdentifier = @"BorderTableViewCell";
    static NSString *LabelTextIdentifier = @"LabelTextTableViewCell";
    static NSString *LabelSwitchIdentifier = @"LabelSwitchTableViewCell";
    static NSString *LabelIdentifier = @"LabelTableViewCell";
    static NSString *ButtonTextTableViewCell = @"ButtonTextTableViewCell";
    
    
    if (indexPath.row==0 || indexPath.row==1)
    {
        BorderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                                     BorderIdentifier];
        
        if (cell == nil)
        {
            cell = [[BorderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:BorderIdentifier];
        }
        
        if (indexPath.row==0)
        {
        cell.label_textFieldType.text = [settingArray objectAtIndex:indexPath.row];
        cell.label_realName.text = @"Tyler Garcia";
        }
        if (indexPath.row==1)
        {
            cell.label_textFieldType.text = [settingArray objectAtIndex:indexPath.row];
             cell.label_realName.text = @"TylerG";
        }
        
        return cell;
    }
    
   else if (indexPath.row==2)
    {
        LabelAndTxtFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                                     LabelTextIdentifier];
        
        if (cell == nil)
        {
            cell = [[LabelAndTxtFieldTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:LabelTextIdentifier];
        }

        
        cell.label_email.text = @"tylergarcia@gmail.com";
        cell.label_TxtSettingName.text = [settingArray objectAtIndex:indexPath.row];
        return cell;
    }
    
   else if (indexPath.row==3 || indexPath.row==4 || indexPath.row==5 || indexPath.row==6 )
    {
        LabelTableViewCell *cell3 = [tableView dequeueReusableCellWithIdentifier:
                                     LabelIdentifier];
        
        if (cell3 == nil)
        {
            cell3 = [[LabelTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:LabelTextIdentifier];
        }
        
        cell3.label_SettingName.text = [settingArray objectAtIndex:indexPath.row];
        return cell3;
    }
    
   else if (indexPath.row==7)
    {
        LabelSwitchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                                    LabelSwitchIdentifier];
        
        if (cell == nil)
        {
            cell = [[LabelSwitchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:LabelSwitchIdentifier];
        }
        
        cell.label_SwitchSettingName.text = [settingArray objectAtIndex:indexPath.row];
        [cell.switch_notifications setOn:YES];
        return cell;
        
    }
   else if (indexPath.row==8 || indexPath.row==9 || indexPath.row==10 || indexPath.row==11 || indexPath.row==12 )
    {
        LabelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                                    LabelIdentifier];
        
        if (cell == nil)
        {
            cell = [[LabelTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:LabelIdentifier];
        }
        
        cell.label_SettingName.text = [settingArray objectAtIndex:indexPath.row];
        return cell;
    }
    
  else  if (indexPath.row==13)
    {
        LogoutTableViewCell *cell13 = [tableView dequeueReusableCellWithIdentifier:
                                     ButtonTextTableViewCell];
        
        if (cell13 == nil)
        {
            cell13 = [[LogoutTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ButtonTextTableViewCell];
        }
        
        cell13.label_logout.text=[settingArray objectAtIndex:indexPath.row];
        return cell13;
    }
    
    else
    {
    
    }
    
   
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:
(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
     UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if (indexPath.row == 2)
    {
       
        ChangeEmailViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"ChangeEmailViewController"];
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    if (indexPath.row == 3)
    {
        
        ChangePasswordViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    if (indexPath.row == 4)
    {
       ChangeProfileDataViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"ChangeProfileDataViewController"];
        controller.titleSetting = @"location";
        [self.navigationController pushViewController:controller animated:YES];
    }
    if (indexPath.row == 5)
    {
        ChangeProfileDataViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"ChangeProfileDataViewController"];
        controller.titleSetting = @"age";
        [self.navigationController pushViewController:controller animated:YES];
    }
    if (indexPath.row == 6)
    {
        ChangeProfileDataViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"ChangeProfileDataViewController"];
        controller.titleSetting = @"status";
        [self.navigationController pushViewController:controller animated:YES];
    }
    if (indexPath.row == 8)
    {
        ChangeProfileDataViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"ChangeProfileDataViewController"];
        controller.titleSetting = @"Instagram";
        [self.navigationController pushViewController:controller animated:YES];
    }
    if (indexPath.row == 9)
    {
        ChangeProfileDataViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"ChangeProfileDataViewController"];
        controller.titleSetting = @"Twitter";
        [self.navigationController pushViewController:controller animated:YES];
    }
    if (indexPath.row == 10)
    {
        ChangeProfileDataViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"ChangeProfileDataViewController"];
        controller.titleSetting = @"Snapchat";
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    if (indexPath.row == 12)
    {
        
        ContactSupportViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"ContactSupportViewController"];
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    

}

- (IBAction)btnActionBack:(UIBarButtonItem *)sender
{
    NavigationBack
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
