//
//  ContactSupportViewController.h
//  Attnd
//
//  Created by Gobinder Singh Sidhu on 4/15/17.
//  Copyright © 2017 Gobinder Sidhu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactSupportViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{

    NSMutableArray *messageArray;

}
@property (weak, nonatomic) IBOutlet UIView *bottomView;

@property (weak, nonatomic) IBOutlet UITextField *txt_message;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewBottomConstraint;

@property (weak, nonatomic) IBOutlet UITableView *messageTableView;


@end
