//
//  ResetPasswordViewController.m
//  Attnd
//
//  Created by Jatin Harish on 25/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import "AFNetworking.h"
#import "Constant.h"
#import "MBProgressHUD.h"
#import "Common.h"
#import "CallAPIClass.h"


@interface ResetPasswordViewController ()<WebAPIDelegate>
{
    CallAPIClass *objAPI;
}

@end

@implementation ResetPasswordViewController
@synthesize forgotEmail;
- (void)viewDidLoad {
    [super viewDidLoad];

    objAPI = [[CallAPIClass alloc]init];
    objAPI.delegate = self;
    
    UIView *NewPasswordPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    txt_NewPassword.leftView = NewPasswordPadding;
    txt_NewPassword.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *ConPasswordPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    txt_ConPassword.leftView = ConPasswordPadding;
    txt_ConPassword.leftViewMode = UITextFieldViewModeAlways;
    
    txt_NewPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"New Password" attributes:@{NSForegroundColorAttributeName: TextfieldPlaceHolderColor}];
    
    txt_NewPassword.layer.cornerRadius = 5.0;
    
    
    txt_ConPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm Password" attributes:@{NSForegroundColorAttributeName: TextfieldPlaceHolderColor}];
    txt_ConPassword.layer.cornerRadius = 5.0;
    
}

#pragma mark -Button Actions

- (IBAction)btnActionResetPassword:(UIButton *)sender
{
    if([self validate])
    {
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        NSMutableDictionary *ResetPasswordParam = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",forgotEmail], @"email", txt_NewPassword.text, @"password",nil];
        
        [objAPI callServerAPI:ResetPasswordParam ServiceType:@"POSt" ServiceURL:RESETPASSWORD];
        
    }
    
}

- (IBAction)btnActionBack:(UIButton *)sender {
    NavigationBack
}
#pragma  mark - API Response Method
-(void)responseServerData:(NSDictionary *)responcedict
{
    NSLog(@"response: %@",responcedict);
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"success"])
    {
        [[Common sharedCommon] showAlertWithOkButton:@"" message:[NSString stringWithFormat:@"%@",[responcedict valueForKey:@"resMessage"]]];
        
        
    }
    if ([[responcedict valueForKey:@"resStatus"] isEqual:@"error"])
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:[NSString stringWithFormat:@"%@",[responcedict valueForKey:@"resMessage"]]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        
    }
    else
    {
        
    }
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}
#pragma mark - Validation Method
-(BOOL)validate
{
    if (txt_NewPassword.text.length==0 && txt_ConPassword.text.length==0)
    {
        txt_NewPassword.layer.borderWidth = 2.5;
        txt_NewPassword.layer.borderColor = [UIColor redColor].CGColor;
        
        lbl_NewPaswordError.hidden=NO;
        lbl_NewPaswordError.text = @"Password should not be Blank";
        txt_ConPassword.layer.borderWidth = 2.5;
        txt_ConPassword.layer.borderColor = [UIColor redColor].CGColor;
        
        lbl_ConPaswordError.hidden=NO;
        lbl_ConPaswordError.text = @"Password should not be Blank";
        return false;
        
    }
    if (txt_NewPassword.text.length==0 && txt_ConPassword.text.length==0)
    {
        if (txt_NewPassword.text.length==0)
        {
            txt_NewPassword.layer.borderWidth = 2.5;
            txt_NewPassword.layer.borderColor = [UIColor redColor].CGColor;
            
            lbl_NewPaswordError.hidden=NO;
            lbl_NewPaswordError.text = @"Username should not be Blank";
            
            lbl_ConPaswordError.hidden=YES;
            txt_ConPassword.layer.borderWidth = 0;
            txt_ConPassword.layer.borderColor = [UIColor clearColor].CGColor;
        }
        else if (txt_ConPassword.text.length==0)
        {
            txt_ConPassword.layer.borderWidth = 2.5;
            txt_ConPassword.layer.borderColor = [UIColor redColor].CGColor;
            lbl_ConPaswordError.hidden=NO;
            lbl_ConPaswordError.text = @"Password should not be Blank";
            lbl_NewPaswordError.hidden=YES;
            txt_NewPassword.layer.borderWidth = 0;
            txt_NewPassword.layer.borderColor = [UIColor clearColor].CGColor;
            
        }
        
        return false;
        
        
    }
    if (![txt_NewPassword.text isEqualToString:txt_ConPassword.text])
    {
        txt_NewPassword.layer.borderWidth = 2.5;
        txt_NewPassword.layer.borderColor = [UIColor redColor].CGColor;
        
        lbl_NewPaswordError.hidden=NO;
        lbl_NewPaswordError.text = @"Password does not be Match";
        txt_ConPassword.layer.borderWidth = 2.5;
        txt_ConPassword.layer.borderColor = [UIColor redColor].CGColor;
        
        lbl_ConPaswordError.hidden=NO;
        lbl_ConPaswordError.text = @"Password does not be Match";
        return false;
    }
    lbl_NewPaswordError.hidden=YES;
    lbl_ConPaswordError.hidden=YES;
    txt_NewPassword.layer.borderWidth = 0;
    txt_NewPassword.layer.borderColor = [UIColor clearColor].CGColor;
    txt_ConPassword.layer.borderWidth = 0;
    txt_ConPassword.layer.borderColor = [UIColor clearColor].CGColor;
    return true;
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
