//
//  InviteFriendsViewController.h
//  Attnd
//
//  Created by Jatin Harish on 13/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteFriendsViewController : UIViewController
{
    
    IBOutlet UICollectionView *colViewInviteFriends;
    IBOutlet UIButton *btnSend;
    IBOutlet UIButton *btnInviteAll;
    IBOutlet UIView *view_Bottom;
    
}
@end
