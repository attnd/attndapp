//
//  SettingViewController.h
//  Attnd
//
//  Created by Gobinder Singh Sidhu on 4/14/17.
//  Copyright © 2017 Gobinder Sidhu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BorderTableViewCell.h"
#import "LabelAndTxtFieldTableViewCell.h"
#import "LabelTableViewCell.h"
#import "LabelSwitchTableViewCell.h"
#import "LogoutTableViewCell.h"

#import "ChangeEmailViewController.h"
#import "ChangePasswordViewController.h"
#import "ContactSupportViewController.h"

@interface SettingViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{

    NSMutableArray *settingArray;

}

@property (weak, nonatomic) IBOutlet UITableView *tblView_Setting;

@end
