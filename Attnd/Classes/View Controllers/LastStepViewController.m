//
//  LastStepViewController.m
//  Attnd
//
//  Created by Jatin Harish on 11/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import "LastStepViewController.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "Common.h"


@interface LastStepViewController ()
{
    Common *objCommon;
}

@end

@implementation LastStepViewController
@synthesize arrayRegisterData;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    objCommon = [Common getInstance];
    
    
}

#pragma mark -Button Actions
- (IBAction)btnActionLogIn:(UIButton *)sender
{
    NSArray *array = [self.navigationController viewControllers];
    
    [self.navigationController popToViewController:[array objectAtIndex:0] animated:YES];
    
    
}


- (IBAction)btnActionBack:(UIButton *)sender {
    NavigationBack
}
- (IBAction)btnActionFacebookLogin:(UIButton *)sender
{
   
    NSArray *permissions = [[NSArray alloc] initWithObjects:
                            @"email", @"public_profile", @"user_friends",
                            nil];
    FBSDKLoginManager* loginManager;
    if (!loginManager) {
        loginManager = [[FBSDKLoginManager alloc] init];
    }

    [loginManager logInWithReadPermissions:permissions
                        fromViewController:self
                                   handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                       
                                       NSLog(@"Login End");
                                       
                                       FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"first_name,id,friends,age_range,relationship_status,picture"}];
                                       [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
                                       {
                                           
                                           if (error)
                                           {
                                               NSLog(@"Error ==%@ ",error);
                                           
                                           
                                           
                                           
                                           
                                           }
                                         
                                           if (!error && result)
                                           
                                           {
           
                                            
                                               
                                               NSLog(@"registerData %@",arrayRegisterData);
                                               [arrayRegisterData setObject:[result valueForKey:@"id"] forKey:@"facebookID"];
                                               [arrayRegisterData setObject:[[result valueForKey:@"age_range"] valueForKey:@"min"] forKey:@"age"];
                                               [arrayRegisterData setObject:[[[result valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"] forKey:@"profileImageUrl"];
                                               
                                               [arrayRegisterData setObject:@"" forKey:@"location"];
                                               
                                              [arrayRegisterData setObject:@"" forKey:@"relationshipStatus"];
                                               
                                               
                                               
                                               NSArray *temparray = [[result valueForKey:@"friends"] valueForKey:@"data"];
                                               
                                               ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
                                               
                                                objCommon.facebookFriends = [temparray mutableCopy];
                                               
                                               
                                             
                                               
                                               //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
                                               
                                               [self RegisterAPIMethod:temparray registerData:arrayRegisterData];

                                               
                                               
                                           }
                                           else
                                           {
                                               
                                               
                                               
                                               
                                           }
                                       }];
                                   }];
    

    
}

-(void)RegisterAPIMethod:(NSArray *) FriendArray registerData:(NSMutableDictionary *)registerData
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer new];
    
    
    NSString *strConnectionURL = [NSString stringWithFormat:@"%@%@",kBaseUrl,REGISTER];
    
    
    NSDictionary *RegisterParam = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [arrayRegisterData valueForKey:@"email"], @"email",
                                   [arrayRegisterData valueForKey:@"realName"] , @"realName",[arrayRegisterData valueForKey:@"password"], @"password", [arrayRegisterData valueForKey:@"userName"] , @"username", [arrayRegisterData valueForKey:@"facebookID"], @"facebookId",
                                       [arrayRegisterData valueForKey:@"age"], @"age",
                                   [arrayRegisterData valueForKey:@"profileImageUrl"], @"profilePic",
                                   [arrayRegisterData valueForKey:@"location"], @"location",
                                   [arrayRegisterData valueForKey:@"relationshipStatus"], @"relationShipStatus",
                                   nil];
    
    [manager POST:strConnectionURL parameters:RegisterParam progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
         NSLog(@"response: %@",responseObject);
         if ([[responseObject valueForKey:@"resStatus"] isEqual:@"success"])
         {
            PeopleYouMayKnowViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PeopleYouMayKnowViewController"];
             
             controller.arrayFriends = [NSMutableArray arrayWithArray:FriendArray];
             
             controller.registerData = arrayRegisterData;
             
             [self.navigationController pushViewController:controller animated:YES];
             
         }
          if ([[responseObject valueForKey:@"resStatus"] isEqual:@"error"])
          {
              [[Common sharedCommon] showAlertWithOkButton:@"" message:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"resMessage"]]];
          }
         else
         {
             
         }
         
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         
     }
          failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
     {
         NSLog(@"error : %@",error.localizedDescription);
         
         
         
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         
     }];
    
    
    
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
