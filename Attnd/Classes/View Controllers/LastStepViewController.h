//
//  LastStepViewController.h
//  Attnd
//
//  Created by Jatin Harish on 11/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "PeopleYouMayKnowViewController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "FBUserDetails.h"

@interface LastStepViewController : UIViewController



@property (retain,nonatomic) NSMutableDictionary *arrayRegisterData;

@end
