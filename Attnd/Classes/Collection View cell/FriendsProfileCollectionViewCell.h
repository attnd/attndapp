//
//  FriendsProfileCollectionViewCell.h
//  Attnd
//
//  Created by Gobinder Singh Sidhu on 4/14/17.
//  Copyright © 2017 Gobinder Sidhu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendsProfileCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView_FriendPic;
@property (weak, nonatomic) IBOutlet UILabel *label_FriendName;
@property (weak, nonatomic) IBOutlet UIView *viewUnderCell;

@end
