//
//  ProfileCentreCollectionViewCell.h
//  Attnd
//
//  Created by Gobinder Singh Sidhu on 4/21/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileCentreCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_btnBckg;
@property (weak, nonatomic) IBOutlet UILabel *label_name;

@end
