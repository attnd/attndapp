//
//  PhotosCollectionViewCell.h
//  Attnd
//
//  Created by Gobinder Singh Sidhu on 4/14/17.
//  Copyright © 2017 Gobinder Sidhu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotosCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView_photos;
@property (weak, nonatomic) IBOutlet UIButton *btn_deletePhoto;
@property (weak, nonatomic) IBOutlet UIView *viewUnderCell;

@end
