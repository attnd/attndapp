//
//  AddEventCollectionViewCell.h
//  Attnd
//
//  Created by Jatin Harish on 25/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddEventCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgView_HostPic;
@property (strong, nonatomic) IBOutlet UIButton *btnCross;

@end
