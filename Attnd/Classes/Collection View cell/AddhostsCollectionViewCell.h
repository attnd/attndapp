//
//  AddhostsCollectionViewCell.h
//  Attnd
//
//  Created by Jatin Harish on 26/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddhostsCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgView_FriendPic;
@property (strong, nonatomic) IBOutlet UIImageView *imgView_TickMark;
@property (strong, nonatomic) IBOutlet UILabel *lbl_friendsName;
@property (strong, nonatomic) IBOutlet UIButton *btnTickMark;

@end
