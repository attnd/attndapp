//
//  SearchFriendCollectionViewCell.h
//  Attnd
//
//  Created by Jatin Harish on 12/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchFriendCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgView_FriendDP;
@property (strong, nonatomic) IBOutlet UILabel *lbl_friendName;
@end
