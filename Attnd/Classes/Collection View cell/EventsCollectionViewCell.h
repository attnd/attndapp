//
//  EventsCollectionViewCell.h
//  Attnd
//
//  Created by Jatin Harish on 13/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgView_attendingPerson;

@end
