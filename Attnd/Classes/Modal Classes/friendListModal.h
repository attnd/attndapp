//
//  friendListModal.h
//  Attnd
//
//  Created by Jatin Harish on 30/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface friendListModal : NSObject


@property(nonatomic,retain)NSString *friend_name;
@property(nonatomic,retain)NSString *friend_id;
@property(nonatomic,retain)NSString *friend_ProfilePic;




-(id)initWithDictionary:(NSDictionary *)ObjDataDict;


@end
