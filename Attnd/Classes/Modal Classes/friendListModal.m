//
//  friendListModal.m
//  Attnd
//
//  Created by Jatin Harish on 30/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import "friendListModal.h"

@implementation friendListModal
@synthesize friend_id,friend_name,friend_ProfilePic;
-(id)initWithDictionary:(NSDictionary *)ObjDataDict
{
    self.friend_ProfilePic=@"";
    self.friend_name = @"";
    self.friend_id = @"";

    
    
    //get friend  id
    if ([ObjDataDict valueForKey:@"_id"] != [NSNull null] && ![[ObjDataDict objectForKey:@"_id"] isEqualToString:@"null"])
    {
        self.friend_id = [ObjDataDict valueForKey:@"_id"];
        
    }
    //get friend name
    if ([ObjDataDict valueForKey:@"realName"] != [NSNull null] && ![[ObjDataDict objectForKey:@"realName"] isEqualToString:@"null"])
    {
        self.friend_name = [ObjDataDict valueForKey:@"realName"];
        
    }
    //Get Friend Profile Pic
    if ([ObjDataDict valueForKey:@"profilePic"] != [NSNull null] && ![[ObjDataDict objectForKey:@"profilePic"] isEqualToString:@"null"])
    {
        self.friend_ProfilePic = [ObjDataDict valueForKey:@"profilePic"];
        
    }
   
    
   return self;
}

@end

