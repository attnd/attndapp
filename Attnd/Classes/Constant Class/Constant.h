//
//  Constant.h
//  Attnd
//
//  Created by Jatin Harish on 10/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//


//*********************************************
#pragma mark - Urls
//*********************************************

#define kBaseUrl @"http://attend.mobilytedev.com:8080/api/"

//#define kBaseUrl @"http://10.20.1.247:8080/api/"

#define kImgBaseURL @"http://attend.mobilytedev.com:8080"
//#define kImgBaseURL @"http://attend.mobilytedev.com:8080"
//http://10.20.1.247:8080/images/events/1493677557userImage.png

#define KendUrl @"/format/json/"


//*********************************************
#pragma mark - API Token
//*********************************************

#define APITokenURL @"a152e84173914146e4bc4f391sd0f686ebc4f31"

//*********************************************
#pragma mark - Screen sizes Constants
//*********************************************

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6sPLUS (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)



// API List
#define IS_IPHONE (!IS_IPAD)
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPhone)

#define SharedAppDelegate ((YourAppDelegate *)[[UIApplication sharedApplication] delegate])

//*********************************************
#pragma mark - API Integrations Constants
//*********************************************

#define LOGIN @"login"
#define REGISTER @"register"
#define FORGOT @"forgot"
#define VERIFYACCOUNT @"verify"
#define RESETPASSWORD @"reset"
#define ADDEVENT @"events"
#define ADDFRIEND @"friends/facebook-friends"
#define GETPROFILE @"profile"
#define UPLOAD_PROFILEPIC @"profile/profile-pic"
#define GETFRIENDLIST @"friends"
#define CHANGELOCATION @"profile/location"
#define CHANGEAGE @"profile/age"
#define CHANGESTATUS @"profile/relationship"
#define CHANGEINSTA @"profile/instagram"
#define CHANGETWITTER @"profile/twitter"
#define CHANGESNAPCHAT @"profile/snapchat"
#define NOTIFICATIONS @"notifications"

//*********************************************
#pragma mark - NSUserDefaults Constants
//*********************************************

#define DEFAULTS [NSUserDefaults standardUserDefaults]
#define UserID [[NSUserDefaults standardUserDefaults] valueForKey:@"userid"]
#define SecurityToken [[NSUserDefaults standardUserDefaults] valueForKey:@"Securitytoken"]
#define userProfilePic [[NSUserDefaults standardUserDefaults] valueForKey:@"ProfilePic"]



//*********************************************
#pragma mark - Colors Constants
//*********************************************


#define white [UIColor whiteColor]
#define black [UIColor blackColor]
#define red [UIColor redColor];
#define gray [UIColor grayColor];
#define lightgray [UIColor lightGrayColor];
#define TextfieldPlaceHolderColor [UIColor colorWithRed:0.6627 green:0.6980 blue:0.8000 alpha:1.0]


//*********************************************
#pragma mark - App Delegates Constants
//*********************************************

#define APPDELEGATE [AppDelegate sharedAppdelegate]






#define toastTimeDuration 2.0




#pragma mark-
#pragma mark-USERDEFAULTS

#define DEFAULTS_SET(key, value)        [DEFAULTS setObject:value forKey:key]; [[NSUserDefaults standardUserDefaults] synchronize];

#define INSTANTIATE(viewController)     [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:viewController];

#pragma mark-
#pragma mark-RESOLUTION AND ORIANTATION


#define IS_LANDSCAPE                    ([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationLandscapeLeft || [[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationLandscapeRight || UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
//
//#define SCREEN_HEIGHT                   (IS_LANDSCAPE ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)
//#define SCREEN_WIDTH                    (IS_LANDSCAPE ? [[UIScreen mainScreen] bounds].size.height : [[UIScreen mainScreen] bounds].size.width)
//
//#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )


#define kGroupview 100
#define Kforgetpwd 200

#pragma mark-
#pragma mark-UI Constants

#define MENU_BTN_X 10.0f

#define MENU_BTN_iY 15.0f

#define MENU_BTN_WIDTH 120.0f

#define MENU_BTN_HEIGHT  20.0f

#define MENU_BTN_SPACE  10.0f

#define MENU_VIEW_X 160.0f

#define MENU_VIEW_Y 75.0f

#define MENU_VIEW_WIDTH 140.0f

#define MENU_VIEW_HEIGHT  160.0f



///Success messages

#define KSuccessTitleCreateAccount @"Create Account"
#define KSuccessMessageCreateAccount @"Account created successfully."

#define KSuccessTitleRegistration @"Registration"
#define KSuccessMessageRegistrationSuccess @"User registered successfully."
#define KSuccessMessageRegistrationSuccessLogin @"User registered successfully."//Please login to continue.

#define KSuccessTitleAccount @"Account verification"
#define KSuccessMessageAccount @"Your account has been verified successfully."

#define KSuccessTitleEmail @"Success"
#define KSuccessMessageEmail @"Email has been sent successfully."

#define KSuccessTitleInformation @"Success"
#define KSuccessMessageInformation @"Information saved successfully."

#define KSuccessTitleGallery @"Save to Gallery"
#define KSuccessMessageImage @"Image successfully saved to gallery."

#define KSuccessTitleTwitter @"Twitter"
#define KSuccessMessageTwitter @"Tweet successfully posted to Twitter."

#define KSuccessTitleFacebook @"Facebook"
#define KSuccessMessageFacebook @"Message successfully posted to Facebook."

#define KSuccessTitleFbCheckin @"Facebook"
#define KSuccessMessageFbCheckin @"Checkin successfully posted to Facebook."

#define KSuccessTitleFriend @"Friend request"
#define KSuccessMessageFriend @"Friend request sent successfully."

#define KSuccessTitleFriendRequestAccept @"Friend request"
#define KSuccessMessageFriendReqeustAccept @"[User] has accepted your friend request."

//Failure Messages

#define KFailureTitleLoginFailure @"Login Failure"
#define KFailureMessageAuthentication @"User authentication failed. Please try again."

#define KFailureTitleLogin @"Login Failure"
#define KFailureMessageUser @"User name/Password incorrect."

#define kTitleForgotPassword @"Forgot Password"
#define kRecoverdPasswordMessage @"A recovered password is sent to your email Id."

#define KFailureTitleSearch @"Search"
#define KFailureMessageSearch @"No search results."

#define KFailureTitleNetwork @"Network Error"
#define KFailureMessageNetwork @"Error connecting to server."

#define KFailureTitleNetworkConnect @"Network Error"
#define KFailureMessageConnet @"Unable to connect to network."

#define KFailureTitleRequestProcess @"Server/Network Error"
#define KFailureMessageRequestProcess @"Request cannot be processed right now. Please try again later."

#define KFailureTitleFrRequest @"Friend request"
#define KFailureMessageFrRequest @"[User] has rejected your friend request."

//Validation messages
#define KFieldsNotBlank  @"Fields cannot be left blank."
#define KValidationTitleRegistration @"Registration"///Module name (e.g. Registration)
#define KValidationMessageBlank @"First Name is required."//Username (i.e. Field name) can not be left Blank.
#define KValidationMessageLMBlank @"Last Name is required."
#define KValidationTitleFirstName  @"First Name"//@"FirstName is the field name; user can replace field name(e.g. User Name )"
#define KValidationMessageFormating @"Only characters are allowed."///@"FirstName is the field name; user can replace field name(e.g. User Name )"

#define KValidationTitleSecondName @"Last Name"//@"SecondName is the field name; user can replace field name(e.g. User Name )"
#define KValidationMessageCharacterLenght @"Name must be 3 to 50 characters long ."//First Name must be 3 to 50 characters long
#define KValidationlastnameCharacterLenght @"Last Name must be 3 to 50 characters long."
#define KValidationpasswordCharacterLenght @"Password must be 6 to 50 characters long."
#define KValidationzipCharacterLenght @"Zip should be between 5 to 7 digits."
#define KValidationTitleEmailAddress @"Email Address"
#define KValidationMessageEmailAddress @"Please enter valid Email address."
#define KValidationMessageEmailPhoneAddress @"Invalid Email/Phone."
#define KValidationMessageLoginFieldBlank @"Invalid Username or Password."
#define KValidationPasswordTitle @"Password"
#define KValidationPasswordsDntMatch @"Passwords don't match.Try again"
#define KValidationTelephone @"Telephone Number"
#define KValidationMessageTelephone @"Please enter valid telephone number."
#define KValidationMessageEnterEmailAddress @"Please enter Email address."
//Notification messages

#define KNotificationTitleJoinStatus  @"Application Name"
#define KNotificationMessageJoinStatus @"[Your friend name] has joined [application]."

#define KNotification @"Notification"
#define KNotificationMessageReferred @"[User] has referred [App/group/news] to you."

#define KNotificationTitleFriendRequest @"Friend request"
#define KNotificationMessageFriendRequest @"[User] has sent you a friend request."

#define KNotificationTitleFriendAdd @"Friend request"
#define KNotificationMessageFriendAdd @"[User] want to add you as a friend on [App name]."

#define KNotificationTitleStatus @"Status"
#define KNotificationMessageRequestPending @"[Request] status is pending."

//Confirmation messages
#define KConfirmationTitleDelete @"Delete"
#define KConfirmationMessageDelete @"Are you sure you want to delete this account."
#define KConfirmationMessageRecord @"Are you sure you want to delete [information/record]."


//Navigation Back
#define NavigationBack [self.navigationController popViewControllerAnimated:YES];

//Constant for Application Drawer

#define APPDEL ((AppDelegate *)[[UIApplication sharedApplication] delegate])



