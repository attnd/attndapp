//
//  Common.m
//  Attnd
//
//  Created by Jatin Harish on 10/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import "Common.h"

@implementation Common

static Common *instance =nil;
+(Common *)getInstance
{
    @synchronized(self)
    {
        if(instance==nil)
        {
            
            instance= [Common new];
            
        }
    }
    return instance;
}


/*!
 @brief Shared instance
 
 @discussion method use for add loading view
 
 @param nothing
 
 @return nothing
 */

+ (Common *)sharedCommon
{
    
    static Common *sharedInstance = nil;
    if (sharedInstance == nil)
    {
        sharedInstance = [[Common alloc]init];
    }
    
    return sharedInstance;
    
}
/*!
 @brief Alert View For User
 
 @discussion method use for Show Alert View with Title and message Only with Ok Button Only
 
 @param message - alert message , Title - Title of Alert View
 
 @return nothing
 */

-(void)showAlertWithOkButton:(NSString *)title message:(NSString *)message
{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:[NSString stringWithFormat:@"%@", message]
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                }];
    [alert addAction:yesButton];
    
    UIViewController *controller  =  [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    
    [controller presentViewController:alert animated:YES completion:nil];
    
}



@end
