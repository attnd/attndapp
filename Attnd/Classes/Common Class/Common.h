//
//  Common.h
//  Attnd
//
//  Created by Jatin Harish on 10/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Common : NSObject
{
    
}
/*!
controls Used as common all classes
*/
@property(nonatomic,strong)NSString *fromwhichIndex;
@property(nonatomic,strong)NSString *device_Token;
@property(nonatomic,strong)NSMutableArray *facebookFriends;
/*!
 singleton method
 */
+(Common*)getInstance;



/*!
 Shared Instance of Common Class
 */
+(Common *)sharedCommon;


/*!
 Show Alert View
 */
-(void)showAlertWithOkButton : (NSString *)title message:(NSString *)message;


@end
