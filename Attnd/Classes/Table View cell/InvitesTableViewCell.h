//
//  InvitesTableViewCell.h
//  Attnd
//
//  Created by Jatin Harish on 27/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InvitesTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgView_Invites;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Username;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Description;
@property (strong, nonatomic) IBOutlet UIButton *btnAcceptedorRejected;
@property (strong, nonatomic) IBOutlet UIButton *btnAccept;
@property (strong, nonatomic) IBOutlet UIButton *btnReject;





@end
