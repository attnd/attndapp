//
//  BorderTableViewCell.h
//  Attnd
//
//  Created by Gobinder Singh Sidhu on 4/14/17.
//  Copyright © 2017 Gobinder Sidhu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BorderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *label_realName;

@property (weak, nonatomic) IBOutlet UILabel *label_textFieldType;

@end
