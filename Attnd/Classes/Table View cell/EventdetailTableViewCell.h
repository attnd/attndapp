//
//  EventdetailTableViewCell.h
//  Attnd
//
//  Created by Jatin Harish on 13/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventdetailTableViewCell : UITableViewCell


// For event Detail Cell
@property (strong, nonatomic) IBOutlet UIView *viewBackgroundMain;
@property (strong, nonatomic) IBOutlet UILabel *lbl_EventTitle;
@property (strong, nonatomic) IBOutlet UILabel *lbl_EventLocation;
@property (strong, nonatomic) IBOutlet UILabel *lbl_EventDate;
@property (strong, nonatomic) IBOutlet UILabel *lbl_EventTime;

@property (strong, nonatomic) IBOutlet UIImageView *imgView_EventPic;

//*-*-*-*-*-*-*-* For Tell A Friend Cell

@property (strong, nonatomic) IBOutlet UIButton *btnSnapChat;
@property (strong, nonatomic) IBOutlet UIButton *btnInstaGram;
@property (strong, nonatomic) IBOutlet UIButton *btnTwitter;
@property (strong, nonatomic) IBOutlet UIButton *btnFacebook;

//*-*-*-*-*-*-*-*- For Whos Going Cell *-*-*-*-
@property (strong, nonatomic) IBOutlet UICollectionView *collView_WhosGoing;

@property (strong, nonatomic) IBOutlet UIButton *btnViewAllWhosGoing;



//*-*-*-*-*-*-*-*- For HOSTS Cell *-*-*-*-
@property (strong, nonatomic) IBOutlet UICollectionView *collView_Hosts;




//*-*-*-*-*-*-*-*- For HIGHLIGHTS Cell *-*-*-*-
@property (strong, nonatomic) IBOutlet UICollectionView *collView_Highlights;
@end
