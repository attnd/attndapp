//
//  LabelSwitchTableViewCell.h
//  Attnd
//
//  Created by Gobinder Singh Sidhu on 4/14/17.
//  Copyright © 2017 Gobinder Sidhu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LabelSwitchTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UISwitch *switch_notifications;
@property (weak, nonatomic) IBOutlet UILabel *label_SwitchSettingName;
@end
