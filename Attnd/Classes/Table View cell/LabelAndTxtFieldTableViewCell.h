//
//  LabelAndTxtFieldTableViewCell.h
//  Attnd
//
//  Created by Gobinder Singh Sidhu on 4/14/17.
//  Copyright © 2017 Gobinder Sidhu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LabelAndTxtFieldTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label_email;

@property (weak, nonatomic) IBOutlet UILabel *label_TxtSettingName;
@end
