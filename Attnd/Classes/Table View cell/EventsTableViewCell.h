//
//  EventsTableViewCell.h
//  Attnd
//
//  Created by Jatin Harish on 12/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIView *view_Background;


@property (strong, nonatomic) IBOutlet UIImageView *imgView_EventPic;
@property (strong, nonatomic) IBOutlet UILabel *lbl_EventTitle;
@property (strong, nonatomic) IBOutlet UILabel *lbl_No_of_PeopleAttnding;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Status;
@property (strong, nonatomic) IBOutlet UICollectionView *colView_attndingPeople;

@end
