//
//  ActivityTableViewCell.h
//  Attnd
//
//  Created by Jatin Harish on 12/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgView_ProfilePic;
@property (strong, nonatomic) IBOutlet UILabel *lbl_description;
@property (strong, nonatomic) IBOutlet UIImageView *imgView_EventPic;

@end
