//
//  CallAPIClass.h
//  ClassForApp
//
//  Created by Sunil Kumar on 4/27/17.
//  Copyright © 2017 Dharmendra Patel. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol WebAPIDelegate <NSObject>
@optional

//*-*-*-*-*-* Delegates Methods for get responce of web service
- (void)responseServerData:(NSDictionary *)responcedict;
- (void)responseServerDataWithHeader:(NSDictionary *)responcedict;


@end
@interface CallAPIClass : NSObject

//***-*-**-*-*-*-*-* Call API Server Methods
-(void)callServerAPI :(NSMutableDictionary*)parameters ServiceType: (NSString*)serviceType ServiceURL:(NSString *)serviceURL;

-(void)callServerAPIwithHeader :(NSMutableDictionary*)parameters ServiceType: (NSString*)serviceType ServiceURL:(NSString *)serviceURL AutherisationToken:(NSString *)autherisationToken;

-(void)callServerAPIwithMultipart :(NSMutableDictionary*)parameters ServiceType: (NSString*)serviceType ServiceURL:(NSString *)serviceURL AutherisationToken:(NSString *)autherisationToken ImgData:(NSData *)imgData ImgParameter:(NSString *)imgparameter;

@property (nonatomic, weak) id <WebAPIDelegate> delegate;

@end
