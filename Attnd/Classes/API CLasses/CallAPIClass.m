//
//  CallAPIClass.m
//  ClassForApp
//
//  Created by Sunil Kumar on 4/27/17.
//  Copyright © 2017 Dharmendra Patel. All rights reserved.
//

#import "CallAPIClass.h"
#import <AFNetworking/AFNetworking.h>
#import "Constant.h"

@implementation CallAPIClass
@synthesize delegate;



#pragma mark - Call Server API Method without adding Authorisation Token in Header

-(void)callServerAPI :(NSMutableDictionary*)parameters ServiceType: (NSString*)serviceType ServiceURL:(NSString *)serviceURL;
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    if ([serviceType isEqualToString:@"POST"])
    {
        [manager POST:[NSString stringWithFormat:@"%@%@",kBaseUrl,serviceURL] parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation,id responseObject)
         {
             [self.delegate responseServerData:responseObject];
         }
              failure:^(NSURLSessionDataTask *operation, NSError *error)
         {
             
             
             NSLog(@"Error: %@", error);
             
             NSLog(@"Error: %@", error.localizedDescription);
             
         }];
    }
    else if ([serviceType isEqualToString:@"GET"])
    {
        [manager GET:[NSString stringWithFormat:@"%@%@",kBaseUrl,serviceURL] parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation,id responseObject)
         {
             [self.delegate responseServerData:responseObject];
         }
             failure:^(NSURLSessionDataTask *operation, NSError *error)
         {
             NSLog(@"Error: %@", error);
         }];
        
    }
    else if ([serviceType isEqualToString:@"DELETE"])
    {
       
    }
    else if ([serviceType isEqualToString:@"PUT"])
    {
        
    }
}

#pragma mark - Call Server API Method with Authorisation Token in Header

-(void)callServerAPIwithHeader :(NSMutableDictionary*)parameters ServiceType: (NSString*)serviceType ServiceURL:(NSString *)serviceURL AutherisationToken:(NSString *)autherisationToken;
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"%@",autherisationToken] forHTTPHeaderField:@"Authorization"];
    
    if ([serviceType isEqualToString:@"POST"])
    {
        [manager POST:[NSString stringWithFormat:@"%@%@",kBaseUrl,serviceURL] parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation,id responseObject)
         {
             [self.delegate responseServerDataWithHeader:responseObject];
         }
              failure:^(NSURLSessionDataTask *operation, NSError *error)
         {
             NSLog(@"Error: %@", error);
             
         }];
    }
    else if ([serviceType isEqualToString:@"GET"])
    {
        [manager GET:[NSString stringWithFormat:@"%@%@",kBaseUrl,serviceURL] parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation,id responseObject)
         {
             [self.delegate responseServerDataWithHeader:responseObject];
         }
             failure:^(NSURLSessionDataTask *operation, NSError *error)
         {
             NSLog(@"Error: %@", error);
             
         }];
        
    }
    else if ([serviceType isEqualToString:@"DELETE"])
    {
        
    }
    else if ([serviceType isEqualToString:@"PUT"])
    {
        [manager PUT:[NSString stringWithFormat:@"%@%@",kBaseUrl,serviceURL] parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
             
             
             [self.delegate responseServerDataWithHeader:responseObject];
             
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
         {
             
             NSLog(@"Error: %@", error);
             
         }];
    }
}

#pragma mark - Call API Server with Multipart with Header

-(void)callServerAPIwithMultipart :(NSMutableDictionary*)parameters ServiceType: (NSString*)serviceType ServiceURL:(NSString *)serviceURL AutherisationToken:(NSString *)autherisationToken ImgData:(NSData *)imgData ImgParameter:(NSString *)imgparameter
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"%@",autherisationToken] forHTTPHeaderField:@"Authorization"];
    
    if ([serviceType isEqualToString:@"POST"])
    {
        [manager POST:[NSString stringWithFormat:@"%@%@",kBaseUrl,serviceURL] parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            [formData appendPartWithFileData:imgData
                                        name:imgparameter
                                    fileName:@"userImage.png" mimeType:@"image/png"];
        }
         
             progress:nil success:^(NSURLSessionDataTask *operation,id responseObject)
         {
             [self.delegate responseServerData:responseObject];
         }
              failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
         {
             
             NSLog(@"error : %@",error);
         }];
    }
    else if ([serviceType isEqualToString:@"GET"])
    {
        
        
    }
}




@end
