//
//  AppDelegate.m
//  Attnd
//
//  Created by Jatin Harish on 10/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import "AppDelegate.h"
#import "FBLogin.h"
#import "FBAppEvents.h"
#import "Common.h"
#import <UserNotifications/UserNotifications.h>

@interface AppDelegate ()<UNUserNotificationCenterDelegate>
{
     NSString *device_token;
     Common *objCommon;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    sleep(2);
    objCommon = [Common getInstance];
    
    //*-*-*-*-*-*-*-*-*-*-*-** UI Tab bar Color Change
    UIView *view1 = [[UIView alloc]initWithFrame:CGRectMake([UITabBar appearance].frame.origin.x,[UITabBar appearance].frame.origin.y, [[UIScreen mainScreen] bounds].size.width/4, 56)];
    UIImageView *border = [[UIImageView alloc]initWithFrame:CGRectMake(10,view1.frame.size.height-7, view1.frame.size.width-20, 7)];
    border.backgroundColor = [UIColor colorWithRed:0.3255 green:0.5333 blue:0.9725 alpha:1.0];
    [view1 addSubview:border];
    UIImage *img=[self ChangeViewToImage:view1];
    [[UITabBar appearance] setSelectionIndicatorImage:img];
    [[UITabBar appearance] setTintColor: [UIColor colorWithRed:0.3255 green:0.5333 blue:0.9725 alpha:1.0]];
    
    ////////////////////////////*-*-*-*-*-*-*-*-*-*-*-*-*
    
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init]
                                      forBarPosition:UIBarPositionAny
                                          barMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];

    
    //*--***-*-*-*-*-*-*-* Remote Notification Function Calling
    [self remoteNotification];
    //*-*-*-*-*-*-*-*-*
  
    
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
    
    
    return YES;
}




-(UIImage * ) ChangeViewToImage : (UIView *) viewForImage
{
    UIGraphicsBeginImageContext(viewForImage.bounds.size);
    [viewForImage.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    [FBAppEvents sendLaunchEvent];
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    // Add any custom logic here.
    return handled;
}

#pragma mark - Remote Notification Delegates

-(void)remoteNotification
{
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate = self;
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
        if(!error){
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
    }];
}
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
//   3c77d9a45476e73e9afd857cd074f6b6db16cfd2f2dc0bb1498189dda28622ae
    device_token = [[deviceToken description] stringByTrimmingCharactersInSet:
                    [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    device_token = [device_token stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:device_token forKey:@"push_token"];
    NSLog(@"My push_token is:%@",device_token);
    
    
    // Get Instance From Data class
    
    objCommon.device_Token = [NSString stringWithFormat:@"%@",device_token];
}
- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}
@end
