//
//  AppDelegate.h
//  Attnd
//
//  Created by Jatin Harish on 10/04/17.
//  Copyright © 2017 Jatin Harish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property(retain ,nonatomic)UITabBarController *tabBarController;
@end

